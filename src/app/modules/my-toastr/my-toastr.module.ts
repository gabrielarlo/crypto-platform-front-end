import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { MomentModule } from 'ngx-moment';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

@NgModule({
    imports: [
        CommonModule,
        BrowserAnimationsModule,
        ToastrModule.forRoot({
            timeOut: 5000,
            positionClass: 'toast-bottom-right',
            enableHtml: true,
            // preventDuplicates: true,
        }),
        MomentModule,
        InfiniteScrollModule,
    ],
    exports: [
        BrowserAnimationsModule,
        ToastrModule,
        MomentModule,
        InfiniteScrollModule,
    ],
    declarations: []
})
export class MyToastrModule { }
