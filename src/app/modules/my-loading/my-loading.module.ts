import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyLoadingComponent } from './my-loading.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [MyLoadingComponent],
  exports: [MyLoadingComponent]
})
export class MyLoadingModule { }
