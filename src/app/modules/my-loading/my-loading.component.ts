import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-my-loading',
    templateUrl: './my-loading.component.html',
    styleUrls: ['./my-loading.component.scss']
})
export class MyLoadingComponent implements OnInit {
    @Input() loading = false;
    @Input() width = 100;

    constructor() { }

    ngOnInit() {
    }

}
