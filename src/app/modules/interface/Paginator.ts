export interface Paginator {
    items: PaginatorItem;
    to_show: number;
    type?: string;
    scope?: string;
}

export interface PaginatorItem {
    current: number;
    buttons: any;
    total: number;
}
