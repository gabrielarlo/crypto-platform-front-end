import { BridgeService } from './../../services/util/bridge.service';
import { Component, OnInit, Input } from '@angular/core';
import { Paginator } from '../interface/Paginator';

@Component({
    selector: 'app-my-paginator',
    templateUrl: './my-paginator.component.html',
    styleUrls: ['./my-paginator.component.scss']
})
export class MyPaginatorComponent implements OnInit {
    @Input() paginator: Paginator = null;

    constructor(private _bridge: BridgeService) { }

    ngOnInit() {
        console.log('paginator loaded');
    }

    pager($direction, $paginator, $type = null, $scope = null) {
        const data = {
            direction: $direction,
            paginator: $paginator,
            type: $type,
            scope: $scope,
        };
        this._bridge.publish('call-pager', data);
    }

    loadPage($page, $type = null, $scope = null) {
        const data = {
            page: $page,
            type: $type,
            scope: $scope,
        };
        this._bridge.publish('call-loader', data);
    }
}
