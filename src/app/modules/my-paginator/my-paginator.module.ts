import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyPaginatorComponent } from './my-paginator.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [MyPaginatorComponent],
  exports: [MyPaginatorComponent]
})
export class MyPaginatorModule { }
