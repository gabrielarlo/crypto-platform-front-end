export interface ContactInterface {
    user_id: number;
    name: string;
    email: string;
    online_status: {
        type: string;
        text: string;
    };
}
