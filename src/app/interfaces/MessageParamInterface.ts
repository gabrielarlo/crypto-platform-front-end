export interface MessageParamInterface {
    from: number;
    to: number;
    message: string;
    is_private: number;
}
