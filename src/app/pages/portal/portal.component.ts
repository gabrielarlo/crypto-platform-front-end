import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { GlobalService } from '../../services/util/global.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-portal',
    templateUrl: './portal.component.html',
    styleUrls: ['./portal.component.scss']
})
export class PortalComponent implements OnInit {

    constructor(
        private _auth: AuthService,
        private _global: GlobalService,
        private _router: Router,
    ) { }

    ngOnInit() {
    }

    logout() {
        this._auth.logout().subscribe(res => {
            this._global.clearSessions();
            // this._router.navigate(['/login']);
            window.location.replace('/login');
        });
    }

    public goto($system) {
        window.location.replace('/' + $system);
    }
}
