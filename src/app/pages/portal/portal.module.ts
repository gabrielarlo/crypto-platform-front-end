import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PortalComponent } from './portal.component';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService as AuthGuard } from '../auth/auth-guard.service';

const routes: Routes = [
    {
        path: 'portal',
        canActivate: [AuthGuard],
        component: PortalComponent,
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
  ],
    declarations: [PortalComponent]
})
export class PortalModule { }
