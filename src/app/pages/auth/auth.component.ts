import { Component, OnInit } from '@angular/core';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';
import { GlobalService } from '../../services/util/global.service';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-auth',
    templateUrl: './auth.component.html',
    styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {
    email = '';
    password = '';
    rememberme = false;
    helpEmail = 'Valid Email Address';
    helpPassword = 'Your awesome password';
    btnDisable = '';

    constructor(private _auth: AuthService, private _router: Router, private _global: GlobalService, private _toastr: ToastrService) {
        if (this._auth.isAuthenticated()) {
            this._router.navigate(['/portal']);
        }
    }

    ngOnInit() {
    }

    onInput(e) {
        if (e.code === 'Enter') {
            this.login();
        }
    }

    login() {
        this.btnDisable = 'disabled';
        this.helpEmail = 'Valid Email Address';
        this.helpPassword = 'Your awesome password';
        let error = 0;
        if (this.email === '') {
            this.helpEmail = `<span class="text-danger">Email is requried</span>`;
            error++;
        }
        if (this.password === '') {
            this.helpPassword = `<span class="text-danger">Password is requried</span>`;
            error++;
        }
        if (error === 0) {
            this._auth.login(this.email, this.password).subscribe(res => {
                if (res.code === 200) {
                    this._global.saveSessions(res.data.token, res.data.user, res.data.user.chat_token, this.rememberme);
                    // this._router.navigate(['/portal']);
                    window.location.replace('/portal');
                } else if (res.code === 401) {
                    this.helpEmail = `<span class="text-danger">${res.message}</span>`;
                    this.helpPassword = '';
                    this.btnDisable = '';
                } else if (res.code === 412) {
                    const type = typeof res.data;
                    const data = res.data;
                    if (type === 'object') {
                        if (data.email !== undefined) {
                            this.helpEmail = `<span class="text-danger">${data.email[0]}</span>`;
                            this.btnDisable = '';
                        }
                        if (data.password !== undefined) {
                            this.helpPassword = `<span class="text-danger">${data.password[0]}</span>`;
                            this.btnDisable = '';
                        }
                    }
                }
            }, err => {
                this.btnDisable = '';
                this._toastr.warning('Server could not be reach', 'Failed');
                console.log(this._global.host);
            });
        } else {
            this.btnDisable = '';
        }
    }

}
