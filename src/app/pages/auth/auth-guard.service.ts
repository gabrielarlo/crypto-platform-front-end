import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../services/util/global.service';

@Injectable({
    providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

    constructor(
        private _auth: AuthService,
        private _location: Location,
        private _global: GlobalService,
    ) { }

    canActivate(): boolean {
        // console.log(this._location.path());
        if (!this._auth.isAuthenticated()) {
            const path = this._location.path();
            this._global.setSystem(path);
            if (path !== '/login') {
                window.location.replace('/login');
                return false;
            }
            return true;
        }
        return true;
    }
}
