import { GlobalService } from './../../services/util/global.service';
import { Injectable } from '@angular/core';
import { Location } from '@angular/common';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    constructor(
        private _global: GlobalService,
        private _location: Location,
    ) { }

    public isAuthenticated(): boolean {
        const path = this._location.path();
        this._global.setSystem(path);
        const token = this._global.api_token;
        if (token === '' || token === null) {
            return false;
        } else {
            return true;
        }
    }

    public login($email, $password) {
        const data = {
            email: $email,
            password: $password,
            origin: 'ManagerFE',
        };
        return this._global.publicPostMethod('/login', data);
    }

    public logout() {
        return this._global.postMethod('/logout', {});
    }
}
