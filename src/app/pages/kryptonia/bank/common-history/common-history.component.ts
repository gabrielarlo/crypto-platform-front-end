import { Component, OnInit, Input } from '@angular/core';
import { GlobalService } from 'src/app/services/util/global.service';
import { Router } from '@angular/router';
import { BankService } from 'src/app/services/model/bank.service';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-common-history',
    templateUrl: './common-history.component.html',
    styleUrls: ['./common-history.component.scss']
})
export class CommonHistoryComponent implements OnInit {
    @Input() histories = null;
    @Input() paginator = null;
    @Input() loading = false;
    $scope = '';
    preview_data = null;
    action_data = null;
    modal = {
        title: 'Preview',
        bg: 'light',
        txt: 'dark'
    };

    cols = null;

    constructor(private _global: GlobalService, private _router: Router, private Bank: BankService, private _toastr: ToastrService) {
    }

    ngOnInit() {

    }

    @Input()
    set scope($scope: string) {
        this.$scope = $scope;
        this.offCols();
        if ($scope === 'sup-for-approval') {
            this.cols.user = true;
            this.cols.user_balance = true;
        } else if ($scope === 'btc-for-approval') {
            this.cols.user = true;
        } else if ($scope === 'basic') {
            this.cols.user = true;
            this.cols.user_balance = true;
        } else if ($scope === 'task') {
            this.cols.creator = true;
            this.cols.completer = true;
        } else if ($scope === 'gift') {
            this.cols.sender = true;
            this.cols.receiver = true;
        } else if ($scope === 'referral') {
            this.cols.referrer = true;
            this.cols.referral = true;
        } else if ($scope === 'social-connect') {
            this.cols.user = true;
            this.cols.sm_name = true;
            this.cols.sm_type = true;
        }
    }

    offCols() {
        this.cols = {
            user: false,
            sender: false,
            receiver: false,
            creator: false,
            completer: false,
            referrer: false,
            referral: false,
            sm_name: false,
            sm_type: false,
            user_balance: false,
        };
    }

    gotoAccountBank($user_id) {
        const link = this._global.userBankLink($user_id);
        this._router.navigate([link]);
    }

    preview($item_id) {
        this.preview_data = this.histories[$item_id];
        this.modal = {
            title: 'Preview',
            bg: 'success',
            txt: 'white'
        };
    }

    approve($item_id) {
        this.action_data = this.histories[$item_id];
        this.Bank.basicWithdrawalAction(this.action_data.id, 'approve').subscribe(res => {
            if (res.code === 200) {
                delete this.histories[$item_id];
                this._toastr.success('Successfully Approved', 'Succes');
            } else {
                this._toastr.error(res.message, 'Failed');
            }
        });
    }

    decline($item_id) {
        this.action_data = this.histories[$item_id];
        this.Bank.basicWithdrawalAction(this.action_data.id, 'decline').subscribe(res => {
            if (res.code === 200) {
                delete this.histories[$item_id];
                this._toastr.success('Successfully Declined', 'Succes');
            } else {
                this._toastr.error(res.message, 'Failed');
            }
        });
    }
}
