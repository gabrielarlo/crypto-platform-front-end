import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BankComponent } from './bank.component';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from '../../auth/auth-guard.service';
import { MyPaginatorModule } from 'src/app/modules/my-paginator/my-paginator.module';
import { CommonHistoryComponent } from './common-history/common-history.component';
import { MomentModule } from 'ngx-moment';
import { MyLoadingModule } from 'src/app/modules/my-loading/my-loading.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const routes: Routes = [
    {
        path: 'kryptonia',
        canActivate: [AuthGuardService],
        children: [
            {
                path: 'bank',
                component: BankComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        FormsModule,
        ReactiveFormsModule,
        MyPaginatorModule,
        MomentModule,
        MyLoadingModule,
    ],
    declarations: [BankComponent, CommonHistoryComponent]
})
export class BankModule { }
