import { Component, OnInit, OnDestroy } from '@angular/core';
import { BankService } from 'src/app/services/model/bank.service';
import { GlobalService } from 'src/app/services/util/global.service';
import { Paginator } from 'src/app/modules/interface/Paginator';
import { Subscription } from 'rxjs';
import { BridgeService } from 'src/app/services/util/bridge.service';

@Component({
    selector: 'app-bank',
    templateUrl: './bank.component.html',
    styleUrls: ['./bank.component.scss']
})
export class BankComponent implements OnInit, OnDestroy {
    subscription: Subscription;
    paginator: Paginator = {
        items: null,
        to_show: 3,
        scope: '',
        type: ''
    };
    search_data = {
        search: '',
        scope: '',
        type: '',
    };
    histories = null;
    scope = null;
    loading = false;

    navs = [
        'sup-for-approval',
        'btc-for-approval',
        'basic',
        'task',
        'gift',
        'referral',
        'social-connect',
    ];

    master_wallet = 0;

    constructor(private Bank: BankService, private _global: GlobalService, private _bridge: BridgeService) { }

    ngOnInit() {
        this.getMainBalance();
        this.getList('sup-for-approval', 'deposit');
        this.subscription = this._bridge.on('call-pager').subscribe(signal => { this.pager(signal.data.direction, signal.data.paginator); });
        this.subscription = this._bridge.on('call-loader').subscribe(signal => { this.getList(signal.data.scope, signal.data.type, signal.data.page); });
    }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }

    getMainBalance() {
        this.Bank.masterBalance().subscribe(res => {
            if (res.code === 200) {
                this.master_wallet = res.data;
            }
        });
    }

    pager($direction, $paginator) {
        let $page = 1;
        if ($paginator.items.current > 1) {
            if ($direction === 'start') {
                $page = 1;
            } else if ($direction === 'previous') {
                $page = $paginator.items.current - 1;
            }
        }
        if ($paginator.items.current < $paginator.items.total) {
            if ($direction === 'end') {
                $page = $paginator.items.total;
            } else if ($direction === 'next') {
                $page = $paginator.items.current + 1;
            }
        }
        this.getList($paginator.scope, $paginator.type, $page);
    }

    onInput($event) {
        if ($event.code === 'Enter') {
            this.getList(this.search_data.scope, this.search_data.type, 1, this.search_data.search);
        }
    }

    getList($scope, $type, $page = 1, $search = '') {
        this.scope = $scope;
        this.loading = true;
        this.search_data = {
            search: $search,
            scope: $scope,
            type: $type,
        };
        this.Bank.generalHistories($scope, $type, $page, $search).subscribe(res => {
            if (res.code === 200) {
                this.paginator = this._global.setPaginator(res.data.total, res.data.limit, $page, $scope, $type);

                if ($scope === 'sup-for-approval') {
                    this.histories = [];
                    for (const item of res.data.items) {
                        item['transaction_type'] = 'SUP Withdrawal';
                        this.histories[item.id] = item;
                    }
                    this.histories.reverse();
                    this.loading = false;
                } else if ($scope === 'btc-for-approval') {
                    this.histories = [];
                    for (const item of res.data.items) {
                        item['transaction_type'] = 'BTC Withdrawal';
                        this.histories[item.id] = item;
                    }
                    this.histories.reverse();
                    this.loading = false;
                } else if ($scope === 'basic') {
                    this.histories = [];
                    for (const item of res.data.items) {
                        item['transaction_type'] = 'SUP ' + ($type === 'deposit' ? 'Deposit' : ($type === 'refund' ? 'Refund' : 'Withdrawal'));
                        this.histories[item.id] = item;
                    }
                    this.histories.reverse();
                    this.loading = false;
                } else if ($scope === 'task') {
                    this.histories = [];
                    for (const item of res.data.items) {
                        item['transaction_type'] = 'Task ' + ($type === 'deposit' ? 'Completed' : 'Revoked');
                        this.histories[item.id] = item;
                    }
                    this.histories.reverse();
                    this.loading = false;
                } else if ($scope === 'gift') {
                    this.histories = [];
                    for (const item of res.data.items) {
                        item['transaction_type'] = 'Sent Gift';
                        this.histories[item.id] = item;
                    }
                    this.histories.reverse();
                    this.loading = false;
                } else if ($scope === 'referral') {
                    this.histories = [];
                    for (const item of res.data.items) {
                        item['transaction_type'] = 'Referral ' + ($type === 'deposit' ? 'Point' : 'Reward');
                        this.histories[item.id] = item;
                    }
                    this.histories.reverse();
                    this.loading = false;
                } else if ($scope === 'social-connect') {
                    this.histories = [];
                    for (const item of res.data.items) {
                        item['transaction_type'] = 'Connected SM';
                        this.histories[item.id] = item;
                    }
                    this.histories.reverse();
                    this.loading = false;
                }
            } else {
                this.loading = false;
            }
        }, err => {
            if (err.status === 401) {
                this._global.clearSessions();
            }
            this.loading = false;
        });
    }
}
