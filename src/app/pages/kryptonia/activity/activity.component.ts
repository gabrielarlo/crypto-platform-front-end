import { Component, OnInit } from '@angular/core';
import { ActivityService } from '../../../services/model/activity.service';
import { GlobalService } from '../../../services/util/global.service';
import { Paginator } from '../../../modules/interface/Paginator';
import { Subscription } from 'rxjs';
import { BridgeService } from '../../../services/util/bridge.service';

@Component({
    selector: 'app-activity',
    templateUrl: './activity.component.html',
    styleUrls: ['./activity.component.scss']
})
export class ActivityComponent implements OnInit {
    activities: any = null;
    preview_data: any = null;
    subscription: Subscription;
    paginator: Paginator = {
        items: null,
        to_show: 3,
        scope: '',
        type: ''
    };
    selected_field = 'all';

    constructor(protected _Actvity: ActivityService, protected _global: GlobalService, protected _bridge: BridgeService) { }

    ngOnInit() {
        this.getList();
        this.subscription = this._bridge.on('call-pager').subscribe(signal => { this.pager(signal.data.direction, signal.data.paginator); });
        this.subscription = this._bridge.on('call-loader').subscribe(signal => { this.getList(signal.data.scope, signal.data.page); });
    }

    getList($field = 'all', $page = 1, $limit = 10) {
        this._Actvity.list($field, $page, $limit).subscribe(res => {
            if (res.code === 200) {
                this.paginator = this._global.setPaginator(res.data.total, $limit, $page, $field);
                this.activities = res.data.items;
            }
        }, err => {
            if (err.status === 403) {
                this._global.clearSessions();
            }
        });
    }

    pager($direction, $paginator) {
        let $page = 1;
        if ($paginator.items.current > 1) {
            if ($direction === 'start') {
                $page = 1;
            } else if ($direction === 'previous') {
                $page = $paginator.items.current - 1;
            }
        }
        if ($paginator.items.current < $paginator.items.total) {
            if ($direction === 'end') {
                $page = $paginator.items.total;
            } else if ($direction === 'next') {
                $page = $paginator.items.current + 1;
            }
        }
        this.getList($paginator.scope, $page);
    }

    filter($field = 'all') {
        this.getList($field);
    }

    view($item_id) {
        this.preview_data = this.activities[$item_id];
    }
}
