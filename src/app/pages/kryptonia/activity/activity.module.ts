import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivityComponent } from './activity.component';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from '../../auth/auth-guard.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MomentModule } from 'ngx-moment';
import { MyLoadingModule } from '../../../modules/my-loading/my-loading.module';
import { MyPaginatorModule } from '../../../modules/my-paginator/my-paginator.module';

const routes: Routes = [
    {
        path: 'kryptonia',
        canActivate: [AuthGuardService],
        children: [
            {
                path: 'activities',
                component: ActivityComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        FormsModule,
        ReactiveFormsModule,
        MomentModule,
        MyLoadingModule,
        MyPaginatorModule,
    ],
    declarations: [ActivityComponent]
})
export class ActivityModule { }
