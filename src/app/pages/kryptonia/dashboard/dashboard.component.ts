import { Component, OnInit } from '@angular/core';
import { KryptoniaComponent } from '../kryptonia.component';
import { GlobalService } from '../../../services/util/global.service';
import { UserService } from '../../../services/model/user.service';
import { TaskService } from 'src/app/services/model/task.service';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent extends KryptoniaComponent implements OnInit {
    users = {
        all: '-',
        active: '-',
        new: '-',
        requested: '-',
    };
    tasks = {
        all: '-',
        active: '-',
        new: '-',
    };

    constructor(private _global: GlobalService, private User: UserService, private Task: TaskService) {
        super();
    }

    ngOnInit() {
        this.getUserStats();
        this.getTaskStats();
    }

    getUserStats() {
        const scopes = ['all', 'active', 'new', 'requested'];
        scopes.forEach(item => {
            this.User.getStat(item).subscribe(res => {
                if (res.code === 200) {
                    this.users[item] = res.data.total;
                }
            }, err => {
                if (err.status === 401) {
                    this._global.clearSessions();
                }
            });
        });
    }

    getTaskStats() {
        const types = ['all', 'active', 'new'];
        types.forEach(item => {
            this.Task.getStat(item).subscribe(res => {
                if (res.code === 200) {
                    this.tasks[item] = res.data.total;
                }
            }, err => {
                if (err.status === 401) {
                    this._global.clearSessions();
                }
            });
        });
    }

}
