import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from '../../auth/auth-guard.service';

const routes: Routes = [
    {
        path: 'kryptonia',
        component: DashboardComponent,
        canActivate: [AuthGuardService],
        children: [
            {
                path: '',
                redirectTo: '/kryptonia/dashboard',
                pathMatch: 'full',
            },
            {
                path: 'dashboard',
                component: DashboardComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
    ],
    declarations: [DashboardComponent]
})
export class DashboardModule { }
