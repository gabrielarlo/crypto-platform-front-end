import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../../services/model/user.service';
import { ToastrService } from 'ngx-toastr';
import { GlobalService } from '../../../services/util/global.service';
import { ClipboardService } from 'ngx-clipboard';

@Component({
    selector: 'app-user',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
    data: any;
    users: any = [];
    filter = '';
    search_result = '';
    limit = 50;
    page = 1;
    total = 0;
    select_user_id: number;
    reason = '';

    temp: any;
    btn_disabled = '';

    user_avatar = 'assets/images/avatar.png';
    account: any = null;
    bank_address = '';
    balances: any = null;
    balance_names: any = [
        'total',
        'hold',
        'available',
        'pending',
        'bonus',
        'premine',
    ];
    controlOptions: any = null;

    @ViewChild('closeDisableModal') closeDisableModal: ElementRef;

    public tabs: any = [
        'bank',
        'activities',
        'referrals',
    ];

    constructor(protected _route: ActivatedRoute, protected User: UserService, protected _toastr: ToastrService, protected _global: GlobalService, protected _clipboard: ClipboardService, protected _router: Router) {
        this._route.data.subscribe(data => {
            this.data = data;
        });
    }

    ngOnInit() {
        this.temp = {
            email: '',
            disabled: false,
            status_text: '',
            modal_title: '',
            banned: true,
        };
        if (this.data.scope === 'account') {
            this._route.params.subscribe(param => {
                this.showAccount(param.user_id);
            });
        } else {
            this.users = [];
            this.getList(this.data.scope);
        }
    }

    gotoAccount($user_id) {
        const link = this._global.profileLink($user_id);
        this._router.navigateByUrl(link);
    }

    activateTab($tab = 'bank') {
        this.tabs.forEach(elem => {
            this.tabs[elem] = '';
        });
        this.tabs[$tab] = 'active';
    }

    getList($scope, $filter = '', $page = 1) {
        this.User.getList($scope, $filter, $page, this.limit).subscribe(res => {
            if (res.code === 200) {
                this.page = res.data.page;
                this.total = res.data.total;
                if ($filter === '') {
                    this.search_result = `<b>${this.total}</b> users found from <b>'${this.data.scope}'</b>`;
                } else {
                    this.search_result = `<b>${this.total}</b> users found having <i><u>${this.filter}</u></i> from <b>'${this.data.scope}'</b>`;
                }
                res.data.users.forEach(user => {
                    const status = this.User.status(user.verified, user.status, user.agreed, user.ban);
                    const item = {
                        user_id: user.id,
                        name: `${user.name}<br><small>${user.username}</small>`,
                        email: user.email,
                        ip: user.ip,
                        status: `<span class="text-${status.type}">${status.text}</span>`,
                        registered_date: user.created_at,
                        options: {
                            account: 1,
                            profile: 1,
                            sendVerification: status.text === 'unverified' ? 1 : 0,
                            verify: status.text === 'unverified' ? 1 : 0,
                            require_sm: status.text === 'unconfirmed' ? 1 : 0,
                            activate: status.text === 'disabled' ? 1 : (status.text === 'unconfirmed' ? 1 : 0),
                            disable: status.text === 'active' ? 1 : 0,
                            unban: user.ban === 0 ? 0 : 1,
                        },
                    };
                    this.users[user.id] = item;
                    this.users.reverse();
                });
            }
        }, err => {
            if (err.status === 401) {
                this._global.clearSessions();
            }
        });
    }

    onInput(e) {
        if (e.code === 'Enter') {
            this.search();
        }
    }

    search() {
        this.users = [];
        this.getList(this.data.scope, this.filter);
    }

    onScroll() {
        const page = this.page + 1;
        this.getList(this.data.scope, this.filter, page);
    }

    verify($user_id) {
        if (this.btn_disabled === '') {
            this.btn_disabled = 'disabled';
            this.User.verify($user_id).subscribe(res => {
                if (res.code === 200) {
                    let email = '';
                    if (this.users.length > 0) {
                        email = this.users[$user_id].email;
                        delete this.users[$user_id];
                    } else {
                        email = this.account.email;
                    }
                    this.total--;
                    if (this.filter === '') {
                        this.search_result = `<b>${this.total}</b> users found from <b>'${this.data.scope}'</b>`;
                    } else {
                        this.search_result = `<b>${this.total}</b> users found having <i><u>${this.filter}</u></i> from <b>'${this.data.scope}'</b>`;
                    }
                    this._toastr.success(email + '\'s Account Successfully Verified', 'Verification');
                    this.btn_disabled = '';
                    this.showAccount($user_id);
                } else {
                    this._toastr.error(res.message, 'Verification');
                    this.btn_disabled = '';
                }
                this.btn_disabled = '';
            }, err => {
                if (err.status === 401) {
                    this._global.clearSessions();
                }
            });
        }
    }

    activate($user_id) {
        if (this.btn_disabled === '') {
            this.btn_disabled = 'disabled';
            this.User.activate($user_id).subscribe(res => {
                if (res.code === 200) {
                    let email = '';
                    if (this.users.length > 0) {
                        email = this.users[$user_id].email;
                        delete this.users[$user_id];
                    } else {
                        email = this.account.email;
                    }
                    this.total--;
                    if (this.filter === '') {
                        this.search_result = `<b>${this.total}</b> users found from <b>'${this.data.scope}'</b>`;
                    } else {
                        this.search_result = `<b>${this.total}</b> users found having <i><u>${this.filter}</u></i> from <b>'${this.data.scope}'</b>`;
                    }
                    this._toastr.success(email + '\'s Account Successfully Activated', 'Acvitvation');
                    this.btn_disabled = '';
                    this.showAccount($user_id);
                } else {
                    this._toastr.error(res.message, 'Acvitvation');
                    this.btn_disabled = '';
                }
            }, err => {
                if (err.status === 401) {
                    this._global.clearSessions();
                }
            });
        }
    }

    openDisableModal($user_id) {
        this.temp.modal_title = 'Disable Account Confirmation';
        this.select_user_id = $user_id;
        if (this.users.length > 0) {
            this.temp.email = this.users[this.select_user_id].email;
        } else {
            this.temp.email = this.account.email;
        }
    }

    openBanModal($user_id) {
        this.temp.modal_title = 'Ban Account Confirmation';
        this.temp.banned = false;
        this.select_user_id = $user_id;
        if (this.users.length > 0) {
            this.temp.email = this.users[this.select_user_id].email;
        } else {
            this.temp.email = this.account.email;
        }
    }

    openUnban($user_id) {
        this.temp.banned = true;
        this.select_user_id = $user_id;
        if (this.users.length > 0) {
            this.temp.email = this.users[this.select_user_id].email;
        } else {
            this.temp.email = this.account.email;
        }
        this.showAccount(this.select_user_id);
        const disabled = false;
        this.reason = 'unbanning account';
        this.ban_change(disabled);
        this.btn_disabled = '';
    }

    disable($disabled) {
        if (this.btn_disabled === '') {
            if (!$disabled) {
                this.btn_disabled = 'disabled';
                this.temp.status_text = 'Please wait while processing...';
                if (this.reason.trim().length > 0) {
                    this.User.disable(this.select_user_id, this.reason).subscribe(res => {
                        if (res.code === 200) {
                            let email = '';
                            if (this.users.length > 0) {
                                email = this.users[this.select_user_id].email;
                                delete this.users[this.select_user_id];
                            } else {
                                email = this.account.email;
                            }
                            this.showAccount(this.select_user_id);
                            this.total--;
                            this.select_user_id = 0;
                            this.reason = '';
                            this.temp.email = '';
                            this.closeDisableModal.nativeElement.click();
                            if (this.filter === '') {
                                this.search_result = `<b>${this.total}</b> users found from <b>'${this.data.scope}'</b>`;
                            } else {
                                this.search_result = `<b>${this.total}</b> users found having <i><u>${this.filter}</u></i> from <b>'${this.data.scope}'</b>`;
                            }
                            this._toastr.success(email + '\'s Account Successfully Disabled', 'Disabling');
                            this.btn_disabled = '';
                        } else {
                            this._toastr.error(res.message, 'Disabling');
                            this.btn_disabled = '';
                        }
                    }, err => {
                        if (err.status === 401) {
                            this._global.clearSessions();
                        }
                    });
                } else {
                    this.temp.status_text = 'Please provide reason.';
                    this.btn_disabled = '';
                }
            }
        }
    }

    ban_change($disabled) {
        if (this.btn_disabled === '') {
            if (!$disabled) {
                this.btn_disabled = 'disabled';
                this.temp.status_text = 'Please wait while processing...';
                if (this.reason.trim().length > 0) {
                    this.User.banUpdate(this.select_user_id, this.reason).subscribe(res => {
                        if (res.code === 200) {
                            let email = '';
                            if (this.users.length > 0) {
                                email = this.users[this.select_user_id].email;
                                delete this.users[this.select_user_id];
                            } else {
                                email = this.account.email;
                            }
                            this.showAccount(this.select_user_id);
                            this.total--;
                            this.select_user_id = 0;
                            this.reason = '';
                            this.temp.email = '';
                            this.temp.banned = true;
                            this.closeDisableModal.nativeElement.click();
                            if (this.filter === '') {
                                this.search_result = `<b>${this.total}</b> users found from <b>'${this.data.scope}'</b>`;
                            } else {
                                this.search_result = `<b>${this.total}</b> users found having <i><u>${this.filter}</u></i> from <b>'${this.data.scope}'</b>`;
                            }
                            this._toastr.success(email + '\'s Account Successfully ' + (this.temp.banned ? 'Banned' : 'Unbanned'), (this.temp.banned ? 'Ban' : 'Unban'));
                            this.btn_disabled = '';
                        } else {
                            this._toastr.error(res.message, (this.temp.banned ? 'Ban' : 'Unban'));
                            this.btn_disabled = '';
                        }
                    }, err => {
                        if (err.status === 401) {
                            this._global.clearSessions();
                        }
                    });
                } else {
                    this.temp.status_text = 'Please provide reason.';
                    this.btn_disabled = '';
                }
            }
        }
    }

    resendVerification($user_id) {
        if (this.btn_disabled === '') {
            this.btn_disabled = 'disabled';
            this.User.resendVerification($user_id).subscribe(res => {
                if (res.code === 200) {
                    this.account.email_token = res.data.token;
                    this.account.verification_resend = res.data.resent_at;
                    this.account.verification_resend_count = res.data.resent_count;
                    this._toastr.success(`Resent Count: ${res.data.resent_count} at ${res.data.resent_at} <br> Token: <b>${res.data.token}</b>`, 'Verification Resent');
                    this.btn_disabled = '';
                } else {
                    this._toastr.warning(res.message, 'Resending Verification Failed');
                    this.btn_disabled = '';
                }
            }, err => {
                if (err.status === 401) {
                    this._global.clearSessions();
                }
            });
        }
    }

    resentVerificationRecord() {
        let html = '';
        if (this.account.verification_resend_count > 0) {
            html = `Count: <b>${this.account.verification_resend_count}</b> <br> At: ${this.account.verification_resend} <br> Token: <b>${this.account.email_token}</b>`;
        }
        return html;
    }

    requireSM($user_id) {
        console.log($user_id);
        this._toastr.info('TODO');
        // TODO
    }

    showAccount($user_id) {
        this.activateTab();
        this.user_avatar = this._global.getAvatar($user_id);
        this.User.account($user_id).subscribe(res => {
            this.account = res.data.user;
            this.account['last_ip'] = '-';
            this.controlOptions = this.getUserControlOptions();
            this.User.activities($user_id, 1).subscribe(res1 => {
                if (res1.code === 200) {
                    if (res1.data.total > 0) {
                        this.account['last_ip'] = `${res1.data.activities[0].ip} [${res1.data.activities[0].iso}]`;
                    }
                }
            });
        }, err => {
            if (err.status === 401) {
                this._global.clearSessions();
            }
        });
    }

    bankBalance($user_id) {
        this.User.getBankBalances($user_id).subscribe(res => {
            if (res.code === 200) {
                this.bank_address = res.data.bank.address;
                this.balances = res.data.balances;
            }
        }, err => {
            if (err.status === 401) {
                this._global.clearSessions();
            }
        });
    }

    updateUserAvatar() {
        this.user_avatar = 'assets/images/avatar.png';
    }

    copyRefCode($ref_code, $url = false) {
        if ($url === true) {
            this._clipboard.copyFromContent(this._global.referralURL($ref_code));
            this._toastr.success('Referral Link Copied');
        } else {
            this._clipboard.copyFromContent($ref_code);
            this._toastr.success('Referral Code Copied');
        }
    }

    accountType($type) {
        return this.User.accountType($type);
    }

    userStatus($user_id) {
        return this.User.status(this.account.verified, this.account.status, this.account.agreed, this.account.ban);
    }

    getUserControlOptions() {
        const options = {
            verify: 0,
            resend: 0,
            require_sm: 0,
            activate: 0,
            disable: 0,
            ban: 0,
            unban: 0,
        };
        const user = this.account;
        const user_status = this.User.status(user.verified, user.status, user.agreed, user.ban);
        if (user_status.text === 'active') {
            options.disable = 1;
            options.ban = 1;
        } else if (user_status.text === 'disabled') {
            options.activate = 1;
            options.ban = 1;
        } else if (user_status.text === 'soft-banned' || user_status.text === 'hard-banned') {
            options.unban = 1;
        } else if (user_status.text === 'unconfirmed') {
            options.require_sm = 1;
            options.activate = 1;
            options.ban = 1;
        } else if (user_status.text === 'unverified') {
            options.resend = 1;
            options.verify = 1;
            options.ban = 1;
        }
        return options;
    }

    banReasons() {
        let html = '';
        if (this.account.ban_reasons !== null) {
            const ban_reasons = JSON.parse(this.account.ban_reasons);
            for (const reason of ban_reasons) {
                html += `<b>[${reason.datetime}]</b> ${reason.reason} <br>`;
            }
        }
        return html;
    }

    disableReasons() {
        let html = '';
        if (this.account.disable_reasons !== null) {
            const disable_reasons = JSON.parse(this.account.disable_reasons);
            for (const reason of disable_reasons) {
                html += `<b>[${reason.datetime}]</b> ${reason.reason} <br>`;
            }
        }
        return html;
    }
}
