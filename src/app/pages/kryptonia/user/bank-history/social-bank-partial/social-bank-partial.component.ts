import { Component, OnInit, Input } from '@angular/core';
import { BankHistoryComponent } from '../bank-history.component';

@Component({
    selector: 'app-social-bank-partial',
    templateUrl: './social-bank-partial.component.html',
    styleUrls: ['./social-bank-partial.component.scss']
})
export class SocialBankPartialComponent extends BankHistoryComponent implements OnInit {
    @Input() social_histories: any;
    @Input() social_paginator: any;
    preview_data: any = null;
    modal = {
        title: 'Preview',
        bg: 'light',
        txt: 'dark'
    };

    ngOnInit() {
        this._route.params.subscribe(param => {
            this.user_id = param.user_id;
        });
    }

    preview($type, $key) {
        this.preview_data = null;
        const item = this.social_histories[$type][$key];
        if ($type === 'deposits') {
            this.modal = {
                title: 'Deposit Preview',
                bg: 'success',
                txt: 'white'
            };
        } else {
            this.modal = {
                title: 'Withdrawal Preview',
                bg: 'warning',
                txt: 'white'
            };
        }
        this.preview_data = {
            item_id: item.id,
            amount: item.amount,
            social: item.social,
            account: `<span class="text-${item.status_info.type}">${item.status_info.text}</span> ${item.account_name}: <small>${item.account_id}</small>`,
            status: `<span class="text-${item.fund_status.type}">${item.fund_status.text}</span>`,
            description: item.description,
            created_at: item.created_at,
        };
    }
}
