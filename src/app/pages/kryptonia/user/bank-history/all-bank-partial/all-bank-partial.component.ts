import { Component, OnInit, Input } from '@angular/core';
import { BankHistoryComponent } from '../bank-history.component';

@Component({
    selector: 'app-all-bank-partial',
    templateUrl: './all-bank-partial.component.html',
    styleUrls: ['./all-bank-partial.component.scss']
})
export class AllBankPartialComponent extends BankHistoryComponent implements OnInit {
    @Input() all_histories: any;
    @Input() all_paginator: any;
    preview_data: any = null;
    modal = {
        title: 'Preview',
        bg: 'light',
        txt: 'dark'
    };

    ngOnInit() {
        this._route.params.subscribe(param => {
            this.user_id = param.user_id;
        });
    }

    preview($type, $key) {
        this.preview_data = null;
        const item = this.all_histories[$type][$key];
        if ($type === 'deposits') {
            this.modal = {
                title: 'Deposit Preview',
                bg: 'success',
                txt: 'white'
            };
            this.preview_data = {
                item_id: item.item_id,
                amount: item.amount,
                gained_from: item.other_user !== null ? item.other_user.name : '',
                // status: `<span class="text-success">Completed</span>`,
                description: item.type,
                created_at: item.created_at,
            };
        } else {
            this.modal = {
                title: 'Withdrawal Preview',
                bg: 'warning',
                txt: 'white'
            };
            this.preview_data = {
                item_id: item.item_id,
                amount: item.amount,
                provided_to: item.other_user !== null ? item.other_user.name : '',
                // status: `<span class="text-success">Completed</span>`,
                description: item.type,
                created_at: item.created_at,
            };
        }
    }
}
