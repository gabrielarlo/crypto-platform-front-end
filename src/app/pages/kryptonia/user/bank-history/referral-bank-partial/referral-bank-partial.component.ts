import { Component, OnInit, Input } from '@angular/core';
import { BankHistoryComponent } from '../bank-history.component';

@Component({
    selector: 'app-referral-bank-partial',
    templateUrl: './referral-bank-partial.component.html',
    styleUrls: ['./referral-bank-partial.component.scss']
})
export class ReferralBankPartialComponent extends BankHistoryComponent implements OnInit {
    @Input() referral_histories: any;
    @Input() referral_paginator: any;
    preview_data: any = null;
    modal = {
        title: 'Preview',
        bg: 'light',
        txt: 'dark'
    };

    ngOnInit() {
        this._route.params.subscribe(param => {
            this.user_id = param.user_id;
        });
    }

    preview($type, $key) {
        this.preview_data = null;
        const item = this.referral_histories[$type][$key];
        if ($type === 'deposits') {
            this.modal = {
                title: 'Deposit Preview',
                bg: 'success',
                txt: 'white'
            };
        } else {
            this.modal = {
                title: 'Withdrawal Preview',
                bg: 'warning',
                txt: 'white'
            };
        }
        this.preview_data = {
            item_id: item.id,
            amount: item.amount,
            referral: item.referral.name,
            status: `<span class="text-${item.status.type}">${item.status.text}</span>`,
            description: item.description,
            created_at: item.created_at,
        };
    }
}
