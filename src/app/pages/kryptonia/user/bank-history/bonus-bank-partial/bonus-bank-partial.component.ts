import { Component, OnInit, Input } from '@angular/core';
import { BankHistoryComponent } from '../bank-history.component';

@Component({
    selector: 'app-bonus-bank-partial',
    templateUrl: './bonus-bank-partial.component.html',
    styleUrls: ['./bonus-bank-partial.component.scss']
})
export class BonusBankPartialComponent extends BankHistoryComponent implements OnInit {
    @Input() bonus_histories: any;
    @Input() bonus_paginator: any;

    ngOnInit() {
        this._route.params.subscribe(param => {
            this.user_id = param.user_id;
        });
    }
}
