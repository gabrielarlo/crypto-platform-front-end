import { Component, OnInit, Input } from '@angular/core';
import { BankHistoryComponent } from '../bank-history.component';

@Component({
    selector: 'app-trade-bank-partial',
    templateUrl: './trade-bank-partial.component.html',
    styleUrls: ['./trade-bank-partial.component.scss']
})
export class TradeBankPartialComponent extends BankHistoryComponent implements OnInit {
    @Input() trade_histories: any;
    @Input() trade_paginator: any;
    preview_data: any = null;
    modal = {
        title: 'Preview',
        bg: 'light',
        txt: 'dark'
    };

    ngOnInit() {
        this._route.params.subscribe(param => {
            this.user_id = param.user_id;
        });
    }

    preview($type, $key) {
        this.preview_data = null;
        const item = this.trade_histories[$type][$key];
        if ($type === 'deposits') {
            this.modal = {
                title: 'Deposit Preview',
                bg: 'success',
                txt: 'white'
            };
            this.preview_data = {
                item_id: item.id,
                coins: item.coin + ' SUP gained',
                price: item.price + ' BTC',
                total: item.total + ' BTC lost',
                seller: item.seller_info.name,
                status: `<span class="text-success">Completed</span>`,
                created_at: item.created_at,
            };
        } else {
            this.modal = {
                title: 'Withdrawal Preview',
                bg: 'warning',
                txt: 'white'
            };
            this.preview_data = {
                item_id: item.id,
                coins: item.coin + ' SUP lost',
                price: item.price + ' BTC',
                total: item.total + ' BTC gained',
                buyer: item.buyer_info.name,
                status: `<span class="text-success">Completed</span>`,
                created_at: item.created_at,
            };
        }
    }
}
