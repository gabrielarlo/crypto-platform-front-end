import { Component, OnInit, Input } from '@angular/core';
import { BankHistoryComponent } from '../bank-history.component';

@Component({
    selector: 'app-gift-bank-partial',
    templateUrl: './gift-bank-partial.component.html',
    styleUrls: ['./gift-bank-partial.component.scss']
})
export class GiftBankPartialComponent extends BankHistoryComponent implements OnInit {
    @Input() gift_histories: any;
    @Input() gift_paginator: any;
    preview_data: any = null;
    modal = {
        title: 'Preview',
        bg: 'light',
        txt: 'dark'
    };

    ngOnInit() {
        this._route.params.subscribe(param => {
            this.user_id = param.user_id;
        });
    }

    preview($type, $key) {
        this.preview_data = null;
        const item = this.gift_histories[$type][$key];
        if ($type === 'deposits') {
            this.modal = {
                title: 'Deposit Preview',
                bg: 'success',
                txt: 'white'
            };
            this.preview_data = {
                item_id: item.id,
                amount: item.coin,
                sender: item.sender.name,
                status: `<span class="text-success">Completed</span>`,
                memo: item.memo,
                created_at: item.created_at,
            };
        } else {
            this.modal = {
                title: 'Withdrawal Preview',
                bg: 'warning',
                txt: 'white'
            };
            this.preview_data = {
                item_id: item.id,
                amount: item.coin,
                receiver: item.receiver.name,
                status: `<span class="text-success">Completed</span>`,
                memo: item.memo,
                created_at: item.created_at,
            };
        }
    }
}
