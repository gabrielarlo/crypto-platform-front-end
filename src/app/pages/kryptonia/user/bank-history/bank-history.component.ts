import { UserComponent } from './../user.component';
import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-bank-history',
    templateUrl: './bank-history.component.html',
    styleUrls: ['./bank-history.component.scss']
})
export class BankHistoryComponent extends UserComponent implements OnInit {
    user_id: number;
    syncing = false;
    sync_password = '';

    all_histories: any = null;
    all_paginator = {
        deposits: null,
        withdrawals: null,
        to_show: 3,
    };
    basic_histories: any = null;
    basic_paginator = {
        deposits: null,
        withdrawals: null,
        refunds: null,
        to_show: 3,
    };
    task_histories: any = null;
    task_paginator = {
        deposits: null,
        withdrawals: null,
        to_show: 3,
    };
    gift_histories: any = null;
    gift_paginator = {
        deposits: null,
        withdrawals: null,
        to_show: 3,
    };
    bonus_histories: any = null;
    bonus_paginator = {
        deposits: null,
        withdrawals: null,
        to_show: 3,
    };
    referral_histories: any = null;
    referral_paginator = {
        deposits: null,
        withdrawals: null,
        to_show: 3,
    };
    social_histories: any = null;
    social_paginator = {
        deposits: null,
        withdrawals: null,
        to_show: 3,
    };
    trade_histories: any = null;
    trade_paginator = {
        deposits: null,
        withdrawals: null,
        to_show: 3,
    };

    ngOnInit() {
        this._route.params.subscribe(param => {
            this.user_id = param.user_id;
            this.User.account(this.user_id).subscribe(res => {
                if (res.code === 200) {
                    this.account = res.data.user;
                    this.bankBalance(param.user_id);
                    this.user_avatar = this._global.getAvatar(this.user_id);
                }
            }, err => {
                if (err.status === 401) {
                    this._global.clearSessions();
                }
            });
        });
        this.getAll();
    }

    sync() {
        this.syncing = true;
        this.User.syncBalance(this.user_id, this.sync_password).subscribe(res => {
            this.syncing = false;
            if (res.code === 200) {
                this.balances = res.data;
                this._toastr.success(res.message);
            } else if (res.code === 412) {
                this._toastr.error(res.message + ' Password is Required');
            } else if (res.code === 400) {
                this._toastr.error(res.message);
            }
        });
    }

    pager($direction, $paginator, $type, $scope) {
        let $page = 1;
        if ($paginator[$type + 's'].current > 1) {
            if ($direction === 'start') {
                $page = 1;
            } else if ($direction === 'previous') {
                $page = $paginator[$type + 's'].current - 1;
            }
        }
        if ($paginator[$type + 's'].current < $paginator[$type + 's'].total) {
            if ($direction === 'end') {
                $page = $paginator[$type + 's'].total;
            } else if ($direction === 'next') {
                $page = $paginator[$type + 's'].current + 1;
            }
        }
        if ($scope === 'all') {
            this.fetchAll($type, $page);
        } else if ($scope === 'basic') {
            this.fetchBasic($type, $page);
        } else if ($scope === 'task') {
            this.fetchTask($type, $page);
        } else if ($scope === 'gift') {
            this.fetchGift($type, $page);
        } else if ($scope === 'bonus') {
            this.fetchBonus($type, $page);
        } else if ($scope === 'referral') {
            this.fetchReferral($type, $page);
        } else if ($scope === 'social_connect') {
            this.fetchSocial($type, $page);
        } else if ($scope === 'option_trade') {
            this.fetchTrade($type, $page);
        }
    }

    loadPage($page, $type, $scope = 'basic') {
        if ($scope === 'all') {
            this.fetchAll($type, $page);
        } else if ($scope === 'basic') {
            this.fetchBasic($type, $page);
        } else if ($scope === 'task') {
            this.fetchTask($type, $page);
        } else if ($scope === 'gift') {
            this.fetchGift($type, $page);
        } else if ($scope === 'bonus') {
            this.fetchBonus($type, $page);
        } else if ($scope === 'referral') {
            this.fetchReferral($type, $page);
        } else if ($scope === 'social_connect') {
            this.fetchSocial($type, $page);
        } else if ($scope === 'option_trade') {
            this.fetchTrade($type, $page);
        }
    }

    getAll() {
        this.all_histories = [];
        this.all_histories['deposits'] = [];
        this.all_histories['withdrawals'] = [];
        this.fetchAll('deposit');
        this.fetchAll('withdrawal');
    }

    fetchAll($type, $page = 1) {
        this.User.getBankHistories(this.user_id, 'all', $type, $page, 10).subscribe(res => {
            // console.log(res);
            if (res.code === 200) {
                if ($type === 'deposit') {
                    const dep = {
                        total: Math.ceil(res.data.total / res.data.limit),
                        current: res.data.page,
                        buttons: [],
                    };
                    for (let index = dep.current - this.all_paginator.to_show; index <= dep.current + this.all_paginator.to_show; index++) {
                        if (index > 0 && index <= dep.total) {
                            if (index === dep.current) {
                                dep.buttons.push({ num: index, class: 'info' });
                            } else {
                                dep.buttons.push({ num: index, class: 'secondary' });
                            }
                        }
                    }
                    this.all_paginator.deposits = dep;

                    this.all_histories[$type + 's'] = [];
                    for (const deposit of res.data.items) {
                        this.all_histories['deposits'].push(deposit);
                    }
                    // this.all_histories['deposits'].reverse();
                } else if ($type === 'withdrawal') {
                    const wd = {
                        total: Math.ceil(res.data.total / res.data.limit),
                        current: res.data.page,
                        buttons: [],
                    };
                    for (let index = wd.current - this.all_paginator.to_show; index <= wd.current + this.all_paginator.to_show; index++) {
                        if (index > 0 && index <= wd.total) {
                            if (index === wd.current) {
                                wd.buttons.push({ num: index, class: 'info' });
                            } else {
                                wd.buttons.push({ num: index, class: 'secondary' });
                            }
                        }
                    }
                    this.all_paginator.withdrawals = wd;

                    this.all_histories[$type + 's'] = [];
                    for (const withdrawal of res.data.items) {
                        this.all_histories['withdrawals'].push(withdrawal);
                    }
                    // this.all_histories['withdrawals'].reverse();
                }
            }
        }, err => {
            if (err.status === 401) {
                this._global.clearSessions();
            }
        });
    }

    getBasics() {
        this.basic_histories = [];
        this.basic_histories['deposits'] = [];
        this.basic_histories['withdrawals'] = [];
        this.basic_histories['refunds'] = [];
        this.fetchBasic('deposit');
        this.fetchBasic('withdrawal');
        this.fetchBasic('refund');
    }

    fetchBasic($type, $page = 1) {
        this.User.getBankHistories(this.user_id, 'basic', $type, $page, 10).subscribe(res => {
            if (res.code === 200) {
                if ($type === 'deposit') {
                    const dep = {
                        total: Math.ceil(res.data.total / res.data.limit),
                        current: res.data.page,
                        buttons: [],
                    };
                    for (let index = dep.current - this.basic_paginator.to_show; index <= dep.current + this.basic_paginator.to_show; index++) {
                        if (index > 0 && index <= dep.total) {
                            if (index === dep.current) {
                                dep.buttons.push({ num: index, class: 'info' });
                            } else {
                                dep.buttons.push({ num: index, class: 'secondary' });
                            }
                        }
                    }
                    this.basic_paginator.deposits = dep;

                    this.basic_histories[$type + 's'] = [];
                    for (const deposit of res.data.items) {
                        this.basic_histories['deposits'].push(deposit);
                    }
                } else if ($type === 'withdrawal') {
                    const wd = {
                        total: Math.ceil(res.data.total / res.data.limit),
                        current: res.data.page,
                        buttons: [],
                    };
                    for (let index = wd.current - this.basic_paginator.to_show; index <= wd.current + this.basic_paginator.to_show; index++) {
                        if (index > 0 && index <= wd.total) {
                            if (index === wd.current) {
                                wd.buttons.push({ num: index, class: 'info' });
                            } else {
                                wd.buttons.push({ num: index, class: 'secondary' });
                            }
                        }
                    }
                    this.basic_paginator.withdrawals = wd;

                    this.basic_histories[$type + 's'] = [];
                    for (const withdrawal of res.data.items) {
                        withdrawal['bank_status'] = this.User.bankHistoryStatus(withdrawal.status, 'basic');
                        this.basic_histories['withdrawals'].push(withdrawal);
                    }
                } else if ($type === 'refund') {
                    const ref = {
                        total: Math.ceil(res.data.total / res.data.limit),
                        current: res.data.page,
                        buttons: [],
                    };
                    for (let index = ref.current - this.basic_paginator.to_show; index <= ref.current + this.basic_paginator.to_show; index++) {
                        if (index > 0 && index <= ref.total) {
                            if (index === ref.current) {
                                ref.buttons.push({ num: index, class: 'info' });
                            } else {
                                ref.buttons.push({ num: index, class: 'secondary' });
                            }
                        }
                    }
                    this.basic_paginator.refunds = ref;

                    this.basic_histories[$type + 's'] = [];
                    for (const deposit of res.data.items) {
                        this.basic_histories['refunds'].push(deposit);
                    }
                }
            }
        }, err => {
            if (err.status === 401) {
                this._global.clearSessions();
            }
        });
    }

    getTasks() {
        this.task_histories = [];
        this.task_histories['deposits'] = [];
        this.task_histories['withdrawals'] = [];
        this.fetchTask('deposit');
        this.fetchTask('withdrawal');
    }

    fetchTask($type, $page = 1) {
        this.User.getBankHistories(this.user_id, 'task', $type, $page, 10).subscribe(res => {
            if (res.code === 200) {
                if ($type === 'deposit') {
                    const dep = {
                        total: Math.ceil(res.data.total / res.data.limit),
                        current: res.data.page,
                        buttons: [],
                    };
                    for (let index = dep.current - this.task_paginator.to_show; index <= dep.current + this.task_paginator.to_show; index++) {
                        if (index > 0 && index <= dep.total) {
                            if (index === dep.current) {
                                dep.buttons.push({ num: index, class: 'info' });
                            } else {
                                dep.buttons.push({ num: index, class: 'secondary' });
                            }
                        }
                    }
                    this.task_paginator.deposits = dep;

                    this.task_histories[$type + 's'] = [];
                    for (const deposit of res.data.items) {
                        deposit['bank_status'] = this.User.bankHistoryStatus(deposit.reward_status, 'task');
                        this.task_histories['deposits'].push(deposit);
                    }
                } else if ($type === 'withdrawal') {
                    const wd = {
                        total: Math.ceil(res.data.total / res.data.limit),
                        current: res.data.page,
                        buttons: [],
                    };
                    for (let index = wd.current - this.task_paginator.to_show; index <= wd.current + this.task_paginator.to_show; index++) {
                        if (index > 0 && index <= wd.total) {
                            if (index === wd.current) {
                                wd.buttons.push({ num: index, class: 'info' });
                            } else {
                                wd.buttons.push({ num: index, class: 'secondary' });
                            }
                        }
                    }
                    this.task_paginator.withdrawals = wd;

                    this.task_histories[$type + 's'] = [];
                    for (const withdrawal of res.data.items) {
                        withdrawal['bank_status'] = this.User.bankHistoryStatus(withdrawal.reward_status, 'task');
                        this.task_histories['withdrawals'].push(withdrawal);
                    }
                }
            }
        }, err => {
            if (err.status === 401) {
                this._global.clearSessions();
            }
        });
    }

    getGifts() {
        this.gift_histories = [];
        this.gift_histories['deposits'] = [];
        this.gift_histories['withdrawals'] = [];
        this.fetchGift('deposit');
        this.fetchGift('withdrawal');
    }

    fetchGift($type, $page = 1) {
        this.User.getBankHistories(this.user_id, 'gift', $type, $page, 10).subscribe(res => {
            if (res.code === 200) {
                if ($type === 'deposit') {
                    const dep = {
                        total: Math.ceil(res.data.total / res.data.limit),
                        current: res.data.page,
                        buttons: [],
                    };
                    for (let index = dep.current - this.gift_paginator.to_show; index <= dep.current + this.gift_paginator.to_show; index++) {
                        if (index > 0 && index <= dep.total) {
                            if (index === dep.current) {
                                dep.buttons.push({ num: index, class: 'info' });
                            } else {
                                dep.buttons.push({ num: index, class: 'secondary' });
                            }
                        }
                    }
                    this.gift_paginator.deposits = dep;

                    this.gift_histories[$type + 's'] = [];
                    for (const deposit of res.data.items) {
                        this.gift_histories['deposits'].push(deposit);
                    }
                } else if ($type === 'withdrawal') {
                    const wd = {
                        total: Math.ceil(res.data.total / res.data.limit),
                        current: res.data.page,
                        buttons: [],
                    };
                    for (let index = wd.current - this.gift_paginator.to_show; index <= wd.current + this.gift_paginator.to_show; index++) {
                        if (index > 0 && index <= wd.total) {
                            if (index === wd.current) {
                                wd.buttons.push({ num: index, class: 'info' });
                            } else {
                                wd.buttons.push({ num: index, class: 'secondary' });
                            }
                        }
                    }
                    this.gift_paginator.withdrawals = wd;

                    this.gift_histories[$type + 's'] = [];
                    for (const withdrawal of res.data.items) {
                        this.gift_histories['withdrawals'].push(withdrawal);
                    }
                }
            }
        }, err => {
            if (err.status === 401) {
                this._global.clearSessions();
            }
        });
    }

    getBonuses() {
        this.bonus_histories = [];
        this.bonus_histories['deposits'] = [];
        this.fetchBonus('deposit');
    }

    fetchBonus($type, $page = 1) {
        this.User.getBankHistories(this.user_id, 'bonus', $type, $page, 10).subscribe(res => {
            if (res.code === 200) {
                if ($type === 'deposit') {
                    const dep = {
                        total: Math.ceil(res.data.total / res.data.limit),
                        current: res.data.page,
                        buttons: [],
                    };
                    for (let index = dep.current - this.bonus_paginator.to_show; index <= dep.current + this.bonus_paginator.to_show; index++) {
                        if (index > 0 && index <= dep.total) {
                            if (index === dep.current) {
                                dep.buttons.push({ num: index, class: 'info' });
                            } else {
                                dep.buttons.push({ num: index, class: 'secondary' });
                            }
                        }
                    }
                    this.bonus_paginator.deposits = dep;

                    this.bonus_histories[$type + 's'] = [];
                    for (const deposit of res.data.items) {
                        this.bonus_histories['deposits'].push(deposit);
                    }
                }
            }
        }, err => {
            if (err.status === 401) {
                this._global.clearSessions();
            }
        });
    }

    getReferrals() {
        this.referral_histories = [];
        this.referral_histories['deposits'] = [];
        this.fetchReferral('deposit');
    }

    fetchReferral($type, $page = 1) {
        this.User.getBankHistories(this.user_id, 'referral', $type, $page, 10).subscribe(res => {
            if (res.code === 200) {
                if ($type === 'deposit') {
                    const dep = {
                        total: Math.ceil(res.data.total / res.data.limit),
                        current: res.data.page,
                        buttons: [],
                    };
                    for (let index = dep.current - this.referral_paginator.to_show; index <= dep.current + this.referral_paginator.to_show; index++) {
                        if (index > 0 && index <= dep.total) {
                            if (index === dep.current) {
                                dep.buttons.push({ num: index, class: 'info' });
                            } else {
                                dep.buttons.push({ num: index, class: 'secondary' });
                            }
                        }
                    }
                    this.referral_paginator.deposits = dep;

                    this.referral_histories[$type + 's'] = [];
                    for (const deposit of res.data.items) {
                        this.referral_histories['deposits'].push(deposit);
                    }
                }
            }
        }, err => {
            if (err.status === 401) {
                this._global.clearSessions();
            }
        });
    }

    getSocials() {
        this.social_histories = [];
        this.social_histories['deposits'] = [];
        this.fetchSocial('deposit');
    }

    fetchSocial($type, $page = 1) {
        this.User.getBankHistories(this.user_id, 'social_connect', $type, $page, 10).subscribe(res => {
            if (res.code === 200) {
                if ($type === 'deposit') {
                    const dep = {
                        total: Math.ceil(res.data.total / res.data.limit),
                        current: res.data.page,
                        buttons: [],
                    };
                    for (let index = dep.current - this.social_paginator.to_show; index <= dep.current + this.social_paginator.to_show; index++) {
                        if (index > 0 && index <= dep.total) {
                            if (index === dep.current) {
                                dep.buttons.push({ num: index, class: 'info' });
                            } else {
                                dep.buttons.push({ num: index, class: 'secondary' });
                            }
                        }
                    }
                    this.social_paginator.deposits = dep;

                    this.social_histories[$type + 's'] = [];
                    for (const deposit of res.data.items) {
                        this.social_histories['deposits'].push(deposit);
                    }
                }
            }
        }, err => {
            if (err.status === 401) {
                this._global.clearSessions();
            }
        });
    }

    getTrades() {
        this.trade_histories = [];
        this.trade_histories['deposits'] = [];
        this.trade_histories['withdrawals'] = [];
        this.fetchTrade('deposit');
        this.fetchTrade('withdrawal');
    }

    fetchTrade($type, $page = 1) {
        this.User.getBankHistories(this.user_id, 'option_trade', $type, $page, 10).subscribe(res => {
            if (res.code === 200) {
                if ($type === 'deposit') {
                    const dep = {
                        total: Math.ceil(res.data.total / res.data.limit),
                        current: res.data.page,
                        buttons: [],
                    };
                    for (let index = dep.current - this.trade_paginator.to_show; index <= dep.current + this.trade_paginator.to_show; index++) {
                        if (index > 0 && index <= dep.total) {
                            if (index === dep.current) {
                                dep.buttons.push({ num: index, class: 'info' });
                            } else {
                                dep.buttons.push({ num: index, class: 'secondary' });
                            }
                        }
                    }
                    this.trade_paginator.deposits = dep;

                    this.trade_histories[$type + 's'] = [];
                    for (const deposit of res.data.items) {
                        this.trade_histories['deposits'].push(deposit);
                    }
                } else if ($type === 'withdrawal') {
                    const wd = {
                        total: Math.ceil(res.data.total / res.data.limit),
                        current: res.data.page,
                        buttons: [],
                    };
                    for (let index = wd.current - this.trade_paginator.to_show; index <= wd.current + this.trade_paginator.to_show; index++) {
                        if (index > 0 && index <= wd.total) {
                            if (index === wd.current) {
                                wd.buttons.push({ num: index, class: 'info' });
                            } else {
                                wd.buttons.push({ num: index, class: 'secondary' });
                            }
                        }
                    }
                    this.trade_paginator.withdrawals = wd;

                    this.trade_histories[$type + 's'] = [];
                    for (const withdrawal of res.data.items) {
                        this.trade_histories['withdrawals'].push(withdrawal);
                    }
                }
            }
        }, err => {
            if (err.status === 401) {
                this._global.clearSessions();
            }
        });
    }

    cancel($key) {
        const item = this.basic_histories['withdrawals'][$key];
        this.User.basicWithdrawalAction(item.id, 'cancel').subscribe(res => {
            if (res.code === 200) {
                this.basic_histories['withdrawals'][$key].status = 7;
                this.basic_histories['withdrawals'][$key].bank_status.text = 'cancelled (7)';
                this.basic_histories['withdrawals'][$key].bank_status.type = 'danger';
                this._toastr.success('Successfully Cancelled', 'Success');
            } else {
                this._toastr.error('Unable to cancel withdrawal', 'Failed');
            }
        });
    }

    approve($key) {
        const item = this.basic_histories['withdrawals'][$key];
        this.User.basicWithdrawalAction(item.id, 'approve').subscribe(res => {
            if (res.code === 200) {
                this.basic_histories['withdrawals'][$key].status = 2;
                this.basic_histories['withdrawals'][$key].bank_status.text = 'processing';
                this.basic_histories['withdrawals'][$key].bank_status.type = 'primary';
                this._toastr.success('Successfully Approved', 'Success');
            } else {
                this._toastr.error('Unable to approve withdrawal', 'Failed');
            }
        });
    }

    decline($key) {
        const item = this.basic_histories['withdrawals'][$key];
        this.User.basicWithdrawalAction(item.id, 'decline').subscribe(res => {
            if (res.code === 200) {
                this.basic_histories['withdrawals'][$key].status = 11;
                this.basic_histories['withdrawals'][$key].bank_status.text = 'declined';
                this.basic_histories['withdrawals'][$key].bank_status.type = 'danger';
                this._toastr.success('Successfully Declined', 'Success');
            } else {
                this._toastr.error('Unable to decline withdrawal', 'Failed');
            }
        });
    }
}
