import { BankHistoryComponent } from './../bank-history.component';
import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-basic-bank-partial',
    templateUrl: './basic-bank-partial.component.html',
    styleUrls: ['./basic-bank-partial.component.scss']
})
export class BasicBankPartialComponent extends BankHistoryComponent implements OnInit {
    @Input() basic_histories: any;
    @Input() basic_paginator: any;
    preview_data: any = null;
    modal = {
        title: 'Preview',
        bg: 'light',
        txt: 'dark'
    };

    ngOnInit() {
        this._route.params.subscribe(param => {
            this.user_id = param.user_id;
        });
    }

    preview($type, $key) {
        this.preview_data = null;
        const item = this.basic_histories[$type][$key];
            if ($type === 'deposits') {
                this.modal = {
                    title: 'Deposit Preview',
                    bg: 'success',
                    txt: 'white'
                };
                this.preview_data = {
                    item_id: item.id,
                    amount: item.coins,
                    tx_hash: item.txid,
                    status: `<span class="text-success">Received</span>`,
                    created_at: item.created_at,
                };
            } else if ($type === 'refunds') {
                this.modal = {
                    title: 'Refund Preview',
                    bg: 'warning',
                    txt: 'white'
                };
                this.preview_data = {
                    item_id: item.id,
                    amount: item.balance,
                    description: item.description,
                    paymentid: item.paymentid,
                    recaddress: item.recaddress,
                    block: item.block,
                    txid: item.txid,
                    ip: item.ip,
                    ban: item.ban === 1 ? `<span class="text-danger">Banned at ${item.ban_at}</span>` : 'n/a',
                    status: `<span class="text-${item.status_info.type}">${item.status_info.text}</span>`,
                    created_at: item.created_at,
                    updated_at: item.updated_at,
                };
            } else {
                this.modal = {
                    title: 'Withdrawal Preview',
                    bg: 'warning',
                    txt: 'white'
                };
                this.preview_data = {
                    item_id: item.id,
                    amount: item.balance,
                    description: item.description,
                    paymentid: item.paymentid,
                    recaddress: item.recaddress,
                    block: item.block,
                    txid: item.txid,
                    ip: item.ip,
                    ban: item.ban === 1 ? `<span class="text-danger">Banned at ${item.ban_at}</span>` : 'n/a',
                    status: `<span class="text-${item.status_info.type}">${item.status_info.text}</span>`,
                    created_at: item.created_at,
                    updated_at: item.updated_at,
                };
            }
    }
}
