import { BankHistoryComponent } from './../bank-history.component';
import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-task-bank-partial',
    templateUrl: './task-bank-partial.component.html',
    styleUrls: ['./task-bank-partial.component.scss']
})
export class TaskBankPartialComponent extends BankHistoryComponent implements OnInit {
    @Input() task_histories: any;
    @Input() task_paginator: any;
    preview_data: any = null;
    modal = {
        title: 'Preview',
        bg: 'light',
        txt: 'dark'
    };

    ngOnInit() {
        this._route.params.subscribe(param => {
            this.user_id = param.user_id;
        });
    }

    preview($type, $key) {
        this.preview_data = null;
        const item = this.task_histories[$type][$key];
        if ($type === 'deposits') {
            this.modal = {
                title: 'Deposit Preview',
                bg: 'success',
                txt: 'white'
            };
            this.preview_data = {
                item_id: item.id,
                amount: item.reward,
                category: item.task.category,
                task_id: item.task_id,
                title: item.task.title,
                creator: item.creator.name,
                task_created_at: item.task.created_at,
                status: `<span class="text-${item.bank_status.type}">${item.bank_status.text}</span>`,
                completed_at: item.created_at,
                task_expiration_date: item.task.expired_date,
            };
        } else {
            this.modal = {
                title: 'Withdrawal Preview',
                bg: 'warning',
                txt: 'white'
            };
            this.preview_data = {
                item_id: item.id,
                amount: item.reward,
                category: item.task.category,
                task_id: item.task_id,
                title: item.task.title,
                completer: item.completer.name,
                task_created_at: item.task.created_at,
                status: `<span class="text-${item.bank_status.type}">${item.bank_status.text}</span>`,
                completed_at: item.created_at,
                task_expiration_date: item.task.expired_date,
            };
        }
    }
}
