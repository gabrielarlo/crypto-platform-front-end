import { BridgeService } from './../../../../services/util/bridge.service';
import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserCookieService } from 'src/app/services/model/user-cookie.service';
import { GlobalService } from 'src/app/services/util/global.service';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/model/user.service';
import { Subscription } from 'rxjs';
import { Paginator } from 'src/app/modules/interface/Paginator';

@Component({
    selector: 'app-multi-account',
    templateUrl: './multi-account.component.html',
    styleUrls: ['./multi-account.component.scss']
})
export class MultiAccountComponent implements OnInit, OnDestroy {
    subscription: Subscription;
    accounts = null;
    similars = null;
    paginator: Paginator = {
        items: null,
        to_show: 3,
    };
    reason = '';
    temp = {
        item_id: null
    };
    filter = '';
    search_result = '';

    constructor(private UserCookie: UserCookieService, private _global: GlobalService, private _router: Router, private User: UserService, private _toastr: ToastrService, private _bridge: BridgeService) {}

    ngOnInit() {
        this.loadList();
        this.subscription = this._bridge.on('call-pager').subscribe(signal => { this.pager(signal.data.direction, signal.data.paginator.items); });
        this.subscription = this._bridge.on('call-loader').subscribe(signal => { this.loadList(signal.data.page); });
    }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }

    onInput(e) {
        if (e.code === 'Enter') {
            this.search();
        }
    }

    search() {
        this.loadList(1, this.filter);
    }

    loadList($page = 1, $filter = '') {
        this.UserCookie.getList($page, $filter).subscribe(res => {
            if (res.code === 200) {
                if ($filter !== '') {
                    this.search_result = `<b>${res.data.total}</b> results found for <b><u>${$filter}</u></b>`;
                } else {
                    this.search_result = '';
                }
                const paginator = {
                    total: Math.ceil(res.data.total / res.data.limit),
                    current: res.data.page,
                    buttons: [],
                };
                for (let index = paginator.current - this.paginator.to_show; index <= paginator.current + this.paginator.to_show; index++) {
                    if (index > 0 && index <= paginator.total) {
                        if (index === paginator.current) {
                            paginator.buttons.push({ num: index, class: 'info' });
                        } else {
                            paginator.buttons.push({ num: index, class: 'secondary' });
                        }
                    }
                }
                this.paginator.items = paginator;

                this.accounts = [];
                for (const item of res.data.items) {
                    this.accounts[item.id] = item;
                }
            }
        }, err => {
            if (err.status === 401) {
                this._global.clearSessions();
            }
        });
    }

    pager($direction, $paginator) {
        let $page = 1;
        if ($paginator.current > 1) {
            if ($direction === 'start') {
                $page = 1;
            } else if ($direction === 'previous') {
                $page = $paginator.current - 1;
            }
        }
        if ($paginator.current < $paginator.total) {
            if ($direction === 'end') {
                $page = $paginator.total;
            } else if ($direction === 'next') {
                $page = $paginator.current + 1;
            }
        }
        this.loadList($page);
    }

    gotoAccount($item_id) {
        const user_id = this.accounts[$item_id].user_id;
        const link = this._global.profileLink(user_id);
        this._router.navigate([link]);
    }

    openBanModal($item_id) {
        this.temp.item_id = $item_id;
    }

    banAccount() {
        if (this.temp.item_id !== null) {
            const $item_id = this.temp.item_id;
            if (this.reason.trim().length > 0) {
                let user_id = null;
                if (this.accounts[$item_id] !== undefined) {
                    user_id = this.accounts[$item_id].user_id;
                }
                if (this.similars[$item_id] !== undefined) {
                    user_id = this.similars[$item_id].user_id;
                }
                this.User.banUpdate(user_id, this.reason).subscribe(res => {
                    if (res.code === 200) {
                        if (this.accounts[$item_id] !== undefined) {
                            this.accounts[$item_id].user_info.ban = 2;
                            this.accounts[$item_id].user_info.status_info.type = 'danger';
                            this.accounts[$item_id].user_info.status_info.text = 'hard-banned';
                        }
                        if (this.similars[$item_id] !== undefined) {
                            this.similars[$item_id].user_info.ban = 2;
                            this.similars[$item_id].user_info.status_info.type = 'danger';
                            this.similars[$item_id].user_info.status_info.text = 'hard-banned';
                        }
                        this._toastr.success(res.message, 'Success');
                    } else {
                        this._toastr.error(res.message, 'Failed');
                    }
                    this.temp.item_id = null;
                    this.reason = '';
                }, err => {
                    if (err.status === 401) {
                        this._global.clearSessions();
                    }
                });
            } else {
                this._toastr.error('Reason is required', 'Warning');
            }
        } else {
            this._toastr.error('Please reload your browser', 'Warning');
        }
    }

    showSimilar($item_id) {
        this.similars = [];
        const cookie = this.accounts[$item_id].cookie;
        this.UserCookie.getSimilar(cookie, null).subscribe(res => {
            if (res.code === 200) {
                for (const item of res.data) {
                    this.similars[item.id] = item;
                }
            }
        });
    }
}
