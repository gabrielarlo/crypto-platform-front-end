import { Component, OnInit } from '@angular/core';
import { UserComponent } from '../user.component';

@Component({
    selector: 'app-summary',
    templateUrl: './summary.component.html',
    styleUrls: ['./summary.component.scss']
})
export class SummaryComponent extends UserComponent implements OnInit {
    user_id: number;
    activities: any = null;
    referrals: any = null;

    has_more_referrals = false;
    has_mode_activities = false;
    total: any = {
        points: 0,
        rewards: 0,
    };
    referral_item: any = null;

    tabs: any = [
        'bank',
        'activities',
        'referrals',
    ];

    ngOnInit() {
        this._route.params.subscribe(param => {
            this.user_id = param.user_id;
            this.activateTab();
            this.activities = null;
            this.referrals = null;
            this.bankBalance(this.user_id);
        });
    }

    activitiesSummary($reload = false) {
        if (this.activities === null || $reload === true) {
            this.activities = [];
            this.User.activities(this.user_id, 5).subscribe(res => {
                if (res.code === 200) {
                    if (res.data.total > res.data.limit) {
                        this.has_mode_activities = true;
                    }
                    for (const act of res.data.activities) {
                        const item = {
                            datetime: act.created_at,
                            action: act.action,
                            ip: `${act.ip} - ${act.iso}`,
                            device: act.device,
                            ua: act.ua,
                            uri: act.uri,
                            status: act.request_status,
                        };
                        this.activities.push(item);
                    }
                }
            }, err => {
                if (err.status === 401) {
                    this._global.clearSessions();
                }
            });
        }
    }

    referralSummary($reload = false) {
        if (this.referrals === null || $reload === true) {
            this.referrals = [];
            this.User.referrals(this.user_id, 5).subscribe(res => {
                if (res.code === 200) {
                    if (res.data.total > res.data.limit) {
                        this.has_more_referrals = true;
                    }
                    for (const ref of res.data.referrals) {
                        let rewards = 0;
                        for (const reward of ref.referral_rewards) {
                            rewards += reward.reward;
                        }
                        this.total.points += ref.points + ref.second_lvl_points + ref.third_lvl_points;
                        this.total.rewards += rewards;
                        const item = {
                            name: {
                                id: ref.referral_info.id,
                                name: ref.referral_info.name,
                                username: ref.referral_info.username,
                                email: ref.referral_info.email,
                                ref_code: ref.referral_info.ref_code,
                            },
                            status: `<span class="text-${this.User.referral_status(ref.status).type}">${this.User.referral_status(ref.status).text}</span>`,
                            registered_at: ref.referral_info.created_at,
                            points: `<b>${ref.points + ref.second_lvl_points + ref.third_lvl_points}</b>SUP`,
                            rewards: `<b>${rewards}</b>SUP`,
                            second_level: ref.second_level,
                            user_status: this.User.status(ref.referral_info.verified, ref.referral_info.status, ref.referral_info.agreed, ref.referral_info.ban),
                        };
                        this.referrals[ref.id] = item;
                        this.referrals.sort();
                    }
                }
            }, err => {
                if (err.status === 401) {
                    this._global.clearSessions();
                }
            });
        }
    }

    previewReferral($item_id) {
        if (this.referrals.length > 0) {
            this.referral_item = this.referrals[$item_id];
        }
    }
}
