import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserComponent } from './user.component';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from '../../auth/auth-guard.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MyToastrModule } from '../../../modules/my-toastr/my-toastr.module';
import { BankHistoryComponent } from './bank-history/bank-history.component';
import { SummaryComponent } from './summary/summary.component';
import { BasicBankPartialComponent } from './bank-history/basic-bank-partial/basic-bank-partial.component';
import { TaskBankPartialComponent } from './bank-history/task-bank-partial/task-bank-partial.component';
import { GiftBankPartialComponent } from './bank-history/gift-bank-partial/gift-bank-partial.component';
import { BonusBankPartialComponent } from './bank-history/bonus-bank-partial/bonus-bank-partial.component';
import { ReferralBankPartialComponent } from './bank-history/referral-bank-partial/referral-bank-partial.component';
import { SocialBankPartialComponent } from './bank-history/social-bank-partial/social-bank-partial.component';
import { TradeBankPartialComponent } from './bank-history/trade-bank-partial/trade-bank-partial.component';
import { AllBankPartialComponent } from './bank-history/all-bank-partial/all-bank-partial.component';
import { MultiAccountComponent } from './multi-account/multi-account.component';
import { MyPaginatorModule } from 'src/app/modules/my-paginator/my-paginator.module';

const routes: Routes = [
    {
        path: 'kryptonia',
        canActivate: [AuthGuardService],
        children: [
            {
                path: 'user',
                children: [
                    {
                        path: 'new',
                        component: UserComponent,
                        data: { scope: 'new' }
                    },
                    {
                        path: 'active',
                        component: UserComponent,
                        data: { scope: 'active' }
                    },
                    {
                        path: 'unverified',
                        component: UserComponent,
                        data: { scope: 'unverified' }
                    },
                    {
                        path: 'unconfirmed',
                        component: UserComponent,
                        data: { scope: 'unconfirmed' }
                    },
                    {
                        path: 'requested',
                        component: UserComponent,
                        data: { scope: 'requested' }
                    },
                    {
                        path: 'disabled',
                        component: UserComponent,
                        data: { scope: 'disabled' }
                    },
                    {
                        path: 'banned',
                        component: UserComponent,
                        data: { scope: 'banned' }
                    },
                    {
                        path: 'all',
                        component: UserComponent,
                        data: { scope: 'all' }
                    },
                    {
                        path: ':user_id',
                        children: [
                            {
                                path: 'account',
                                component: UserComponent,
                                data: { scope: 'account' }
                            },
                            {
                                path: 'bank-history',
                                component: BankHistoryComponent,
                            }
                        ]
                    }
                ]
            },
            {
                path: 'multi-account',
                component: MultiAccountComponent,
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        FormsModule,
        ReactiveFormsModule,
        MyToastrModule,
        MyPaginatorModule,
    ],
    declarations: [
        UserComponent,
        BankHistoryComponent,
        SummaryComponent,
        BasicBankPartialComponent,
        TaskBankPartialComponent,
        GiftBankPartialComponent,
        BonusBankPartialComponent,
        ReferralBankPartialComponent,
        SocialBankPartialComponent,
        TradeBankPartialComponent,
        AllBankPartialComponent,
        MultiAccountComponent,
    ]
})
export class UserModule { }
