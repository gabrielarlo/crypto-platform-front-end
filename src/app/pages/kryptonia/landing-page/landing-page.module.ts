import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TrustedBusinessComponent } from './trusted-business/trusted-business.component';
import { AffiliatedSitesComponent } from './affiliated-sites/affiliated-sites.component';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from '../../auth/auth-guard.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MomentModule } from 'ngx-moment';
import { TestimonialComponent } from './testimonial/testimonial.component';

const routes: Routes = [
    {
        path: 'kryptonia',
        canActivate: [AuthGuardService],
        children: [
            {
                path: 'landing-page',
                children: [
                    {
                        path: 'trusted-business',
                        component: TrustedBusinessComponent
                    },
                    {
                        path: 'affiliated-sites',
                        component: AffiliatedSitesComponent
                    },
                    {
                        path: 'testimonials',
                        component: TestimonialComponent
                    }
                ]
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        FormsModule,
        ReactiveFormsModule,
        MomentModule
    ],
    declarations: [TrustedBusinessComponent, AffiliatedSitesComponent, TestimonialComponent]
})
export class LandingPageModule { }
