import { Component, OnInit } from '@angular/core';
import { SiteService } from 'src/app/services/model/site.service';
import { GlobalService } from 'src/app/services/util/global.service';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-affiliated-sites',
    templateUrl: './affiliated-sites.component.html',
    styleUrls: ['./affiliated-sites.component.scss']
})
export class AffiliatedSitesComponent implements OnInit {
    sites = null;
    preview_data = null;
    new_data = null;

    constructor(private Site: SiteService, private _global: GlobalService, private _toaster: ToastrService) { }

    ngOnInit() {
        this.loadList();
    }

    loadList($page = 1) {
        this.Site.list($page).subscribe(res => {
            if (res.code === 200) {
                this.sites = [];
                for (const item of res.data.items) {
                    this.sites[item.id] = item;
                }
            }
        }, err => {
            if (err.status === 401) {
                this._global.clearSessions();
            }
        });
    }

    addModal() {
        this.new_data = {
            site_name: '',
            site_url: '',
        };
    }

    add() {
        if (this.new_data.site_name.trim().length === 0) {
            this._toaster.warning('Site name is required', 'Required');
        } else {
            if (this.new_data.site_url.trim().length === 0) {
                this._toaster.warning('Site URL is required', 'Required');
            } else {
                this.Site.add(this.new_data.site_name, this.new_data.site_url).subscribe(res => {
                    if (res.code === 201) {
                        this.new_data.site_name = '';
                        this.new_data.site_url = '';
                        this.sites[res.data.id] = res.data;
                        this._toaster.success('Site Info is Added', 'Success');
                    } else {
                        this._toaster.error(res.message, 'Failed');
                    }
                }, err => {
                    this._toaster.error('Error: ' + err.status, 'Error');
                });
            }
        }
    }

    editModal($item_id) {
        this.preview_data = this.sites[$item_id];
    }

    update() {
        if (this.preview_data.site_name.trim().length === 0) {
            this._toaster.warning('Site name is required', 'Required');
        } else {
            if (this.preview_data.site_url.trim().length === 0) {
                this._toaster.warning('Site URL is required', 'Required');
            } else {
                this.Site.update(this.preview_data.id, this.preview_data.site_name, this.preview_data.site_url).subscribe(res => {
                    if (res.code === 200) {
                        this.preview_data.updated_at = res.data.updated_at;
                        this._toaster.success('Site Info is Updated', 'Success');
                    } else {
                        this._toaster.error('Unable to update site info', 'Failed');
                    }
                }, err => {
                    this._toaster.error('Error: ' + err.status, 'Error');
                });
            }
        }
    }

    changeStatus($item_id) {
        this.Site.statusChange($item_id).subscribe(res => {
            if (res.code === 200) {
                this.sites[$item_id].status = res.data.status;
                this._toaster.success('Site is successfully ' + (res.data.status === 1 ? 'activated' : 'trashed'), 'Success');
            } else {
                this._toaster.error('Unable to process', 'Failed');
            }
        });
    }
}
