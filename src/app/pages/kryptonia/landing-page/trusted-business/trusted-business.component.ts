import { Component, OnInit } from '@angular/core';
import { GlobalService } from 'src/app/services/util/global.service';
import { BusinessService } from 'src/app/services/model/business.service';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-trusted-business',
    templateUrl: './trusted-business.component.html',
    styleUrls: ['./trusted-business.component.scss']
})
export class TrustedBusinessComponent implements OnInit {
    businesses = null;
    preview_data = null;
    new_data = null;
    business_logo = null;

    constructor(private _global: GlobalService, private Business: BusinessService, private _toaster: ToastrService) { }

    ngOnInit() {
        this.loadList();
    }

    loadList($page = 1) {
        this.Business.list($page).subscribe(res => {
            if (res.code === 200) {
                this.businesses = [];
                for (const item of res.data.items) {
                    item['logo'] = this.Business.getLogo(item.business_logo);
                    item['thumbnail'] = this.Business.getLogo(item.business_logo, false);
                    this.businesses[item.id] = item;
                }
            }
        }, err => {
            if (err.status === 401) {
                this._global.clearSessions();
            }
        });
    }

    errorLogo($item_id) {
        this.businesses[$item_id]['logo'] = '/assets/images/no-image.png';
        this.businesses[$item_id]['thumbnail'] = '/assets/images/no-image.png';
    }

    addModal() {
        this.new_data = {
            business_name: '',
        };
    }

    add() {
        if (this.new_data.business_name.trim().length === 0) {
            this._toaster.warning('Business name is required', 'Required');
        } else {
            this.Business.add(this.new_data.business_name).subscribe(res => {
                console.log(res);
                if (res.code === 201) {
                    this.new_data.business_name = '';
                    this.businesses[res.data.id] = res.data;
                    this._toaster.success('Business Info is Added', 'Success');
                } else {
                    this._toaster.error('Unable to update business info', 'Failed');
                }
            }, err => {
                this._toaster.error('Error: ' + err.status, 'Error');
            });
        }
    }

    editModal($item_id) {
        this.preview_data = this.businesses[$item_id];
    }

    update() {
        if (this.preview_data.business_name.trim().length === 0) {
            this._toaster.warning('Business name is required', 'Required');
        } else {
            this.Business.update(this.preview_data.id, this.preview_data.business_name).subscribe(res => {
                if (res.code === 200) {
                    this.preview_data.updated_at = res.data.updated_at;
                    this._toaster.success('Business Info is Updated', 'Success');
                } else {
                    this._toaster.error('Unable to update business info', 'Failed');
                }
            }, err => {
                this._toaster.error('Error: ' + err.status, 'Error');
            });
        }
    }

    onLogoChange(event) {
        const reader = new FileReader();
        if (event.target.files && event.target.files.length > 0) {
            const file = event.target.files[0];
            reader.readAsDataURL(file);
            reader.onload = () => {
                this.business_logo = file;
            };
        }
    }

    change() {
        this.Business.changeLogo(this.preview_data.id, this.business_logo).subscribe(res => {
            if (res.code === 200) {
                this.preview_data['logo'] = this.Business.getLogo(this.preview_data.business_logo + '?' + this.preview_data.updated_at);
                this.preview_data['thumbnail'] = this.Business.getLogo(this.preview_data.business_logo + '?' + this.preview_data.updated_at, false);
                this._toaster.success('Business logo is successfully changed');
            } else {
                this._toaster.error('Unavle to change business logo');
            }
        });
    }

    changeStatus($item_id) {
        this.Business.statusChange($item_id).subscribe(res => {
            if (res.code === 200) {
                this.businesses[$item_id].status = res.data.status;
                this._toaster.success('Business is successfully ' + (res.data.status === 1 ? 'activated' : 'trashed'), 'Success');
            } else {
                this._toaster.error('Unable to process', 'Failed');
            }
        });
    }
}
