import { Component, OnInit } from '@angular/core';
import { TestimonialService } from 'src/app/services/model/testimonial.service';
import { GlobalService } from 'src/app/services/util/global.service';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-testimonial',
    templateUrl: './testimonial.component.html',
    styleUrls: ['./testimonial.component.scss']
})
export class TestimonialComponent implements OnInit {
    testimonials = null;
    limits = {
        title: 30,
        content: 1000
    };
    counter = {
        title: '0/' + this.limits.title,
        content: '0/' + this.limits.content,
    };
    new_data = null;
    update_data = null;
    preview_data = null;

    constructor(private Testimonial: TestimonialService, private _global: GlobalService, private _toastr: ToastrService) { }

    ngOnInit() {
        this.loadList();
    }

    loadList($page = 1) {
        this.Testimonial.list($page, 100).subscribe(res => {
            if (res.code === 200) {
                this.testimonials = [];
                if (res.data.total > 0) {
                    for (const item of res.data.items) {
                        this.testimonials[item.id] = item;
                    }
                }
            }
        }, err => {
            if (err.status === 401) {
                this._global.clearSessions();
            }
        });
    }

    addModal() {
        this.new_data = {
            title: '',
            content: '',
            name: '',
        };
    }

    checkInput($event, $part, $what_data) {
        let data = this.new_data;
        if ($what_data === 'update') {
            data = this.update_data;
        }
        if ($part === 'title') {
            if (data.title.length <= this.limits.title) {
                this.counter.title = `<span class="text-success">${data.title.length}/${this.limits.title}</span>`;
            } else {
                this.counter.title = `<span class="text-danger">${data.title.length}/${this.limits.title}</span>`;
            }
        } else if ($part === 'content') {
            if (data.content.length <= this.limits.content) {
                this.counter.content = `<span class="text-success">${data.content.length}/${this.limits.content}</span>`;
            } else {
                this.counter.content = `<span class="text-danger">${data.content.length}/${this.limits.content}</span>`;
            }
        }
    }

    add() {
        if (this.new_data.title.trim().length === 0) {
            this._toastr.warning('Title is required', 'Warning');
        } else {
            if (this.new_data.content.trim().length === 0) {
                this._toastr.warning('Content is required', 'Warning');
            } else {
                if (this.new_data.name.trim().length === 0) {
                    this._toastr.warning('Author is required', 'Warning');
                } else {
                    if (this.new_data.title.length > this.limits.title) {
                        this._toastr.warning('Title must be equal or less than ' + this.limits.title  + ' characters', 'Warning');
                    } else {
                        if (this.new_data.content.length > this.limits.content) {
                            this._toastr.warning('Content must be equal or less than ' + this.limits.content + ' characters', 'Warning');
                        } else {
                            this.Testimonial.add(this.new_data.title, this.new_data.content, this.new_data.name).subscribe(res => {
                                if (res.code === 201) {
                                    this.testimonials[res.data.id] = res.data;
                                    this.addModal();
                                    this._toastr.success('Successfully Added', 'Success');
                                } else {
                                    this._toastr.error(res.message, 'Failed');
                                }
                            });
                        }
                    }
                }
            }
        }
    }

    editModal($item_id) {
        this.update_data = this.testimonials[$item_id];
    }

    update() {
        if (this.update_data.title.trim().length === 0) {
            this._toastr.warning('Title is required', 'Warning');
        } else {
            if (this.update_data.content.trim().length === 0) {
                this._toastr.warning('Content is required', 'Warning');
            } else {
                if (this.update_data.name.trim().length === 0) {
                    this._toastr.warning('Author is required', 'Warning');
                } else {
                    if (this.update_data.title.length > this.limits.title) {
                        this._toastr.warning('Title must be equal or less than ' + this.limits.title + ' characters', 'Warning');
                    } else {
                        if (this.update_data.content.length > this.limits.content) {
                            this._toastr.warning('Content must be equal or less than ' + this.limits.content + ' characters', 'Warning');
                        } else {
                            this.Testimonial.update(this.update_data.id, this.update_data.title, this.update_data.content, this.update_data.name).subscribe(res => {
                                if (res.code === 200) {
                                    this.testimonials[res.data.id] = res.data;
                                    this.update_data = res.data;
                                    this._toastr.success('Successfully Updated', 'Success');
                                } else {
                                    this._toastr.error(res.message, 'Failed');
                                }
                            });
                        }
                    }
                }
            }
        }
    }

    trashModal($item_id) {
        this.preview_data = this.testimonials[$item_id];
    }

    trash() {
        this.Testimonial.statusChange(this.preview_data.id).subscribe(res => {
            if (res.code === 200) {
                delete this.testimonials[res.data.id];
                this.preview_data = null;
                this._toastr.success('Successfully Trashed', 'Success');
            } else {
                this._toastr.error(res.message, 'Failed');
            }
        });
    }
}
