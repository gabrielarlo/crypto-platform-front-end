import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardModule } from './dashboard/dashboard.module';
import { KryptoniaComponent } from './kryptonia.component';
import { UserModule } from './user/user.module';
import { FaqModule } from './faq/faq.module';
import { LandingPageModule } from './landing-page/landing-page.module';
import { BankModule } from './bank/bank.module';
import { TaskModule } from './task/task.module';
import { ChatModule } from './chat/chat.module';
import { ActivityModule } from './activity/activity.module';

@NgModule({
    imports: [
        CommonModule,
        DashboardModule,
        UserModule,
        FaqModule,
        LandingPageModule,
        BankModule,
        TaskModule,
        ChatModule,
        ActivityModule,
    ],
    declarations: [KryptoniaComponent]
})
export class KryptoniaModule { }
