import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FaqComponent } from './faq.component';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from '../../auth/auth-guard.service';
import { MyToastrModule } from 'src/app/modules/my-toastr/my-toastr.module';
import { MyPaginatorModule } from 'src/app/modules/my-paginator/my-paginator.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxWigModule } from 'ngx-wig';
import { polyfill as keyboardEventKeyPolyfill } from 'keyboardevent-key-polyfill';
import { TextInputAutocompleteModule } from 'angular-text-input-autocomplete';

const routes: Routes = [
    {
        path: 'kryptonia',
        canActivate: [AuthGuardService],
        children: [
            {
                path: 'faq',
                component: FaqComponent,
            }
        ]
    }
];

keyboardEventKeyPolyfill();

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        FormsModule,
        ReactiveFormsModule,
        MyToastrModule,
        MyPaginatorModule,
        NgxWigModule,
        TextInputAutocompleteModule,
    ],
    declarations: [FaqComponent]
})
export class FaqModule { }
