import { Component, OnInit } from '@angular/core';
import { FaqService } from 'src/app/services/model/faq.service';
import { GlobalService } from 'src/app/services/util/global.service';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-faq',
    templateUrl: './faq.component.html',
    styleUrls: ['./faq.component.scss']
})
export class FaqComponent implements OnInit {
    categories = null;
    faqs = null;
    preview_data = null;

    new_data = {
        category: '',
        title: '',
        content: '',
    };

    update_data = {
        id: null,
        category: '',
        title: '',
        content: '',
    };

    constructor(private Faq: FaqService, private _global: GlobalService, private _toastr: ToastrService) { }

    ngOnInit() {
        this.getList();
    }

    getList($page = 1) {
        this.Faq.list($page).subscribe(res => {
            if (res.code === 200) {
                this.categories = [];
                this.faqs = {};
                for (const category of res.data.categories) {
                    this.categories.push(category.category);
                    const items = res.data.items[category.category];
                    this.faqs[category.category] = {
                        items: [],
                        count: 0,
                    };
                    for (const item of items) {
                        this.faqs[category.category].items[item.id] = item;
                        this.faqs[category.category].count++;
                    }
                }
            }
        }, err => {
            if (err.status === 401) {
                this._global.clearSessions();
            }
        });
    }

    preview($category, $item_id) {
        this.preview_data = null;
        this.preview_data = this.faqs[$category].items[$item_id];
    }

    trash($preview_data) {
        this.Faq.deletedChange($preview_data.id).subscribe(res => {
            if (res.code === 200) {
                this.preview_data.deleted_info = res.data.deleted_info;
                this.preview_data.deleter_info = res.data.deleter_info;
                this.preview_data.deleted_at = res.data.deleted_at.date;

                this.faqs[$preview_data.category].items[$preview_data.id].is_deleted = 1;

                this._toastr.success('Successfully Trashed', 'Success');
            } else {
                this._toastr.error('Unable to process', 'Failed');
            }
        }, err => {
            if (err.status === 401) {
                this._global.clearSessions();
            }
        });
    }

    add() {
        if (this.new_data.category.trim().length > 0) {
            if (this.new_data.title.trim().length > 0) {
                if (this.new_data.content.trim().length > 0 && this.new_data.content !== '<br>') {
                    this.Faq.add(this.new_data.category, this.new_data.title, this.new_data.content).subscribe(res => {
                        if (res.code === 201) {
                            if (this.faqs === null) {
                                this.faqs = [];
                                this.faqs[res.data.category] = {
                                    items: [],
                                    count: 1,
                                };
                                this.faqs[res.data.category].items[res.data.id] = res.data;
                            } else {
                                if (this.faqs[res.data.category] === undefined) {
                                    this.categories.push(res.data.category);
                                    this.faqs[res.data.category] = {
                                        items: [],
                                        count: 1,
                                    };
                                    this.faqs[res.data.category].items[res.data.id] = res.data;
                                } else {
                                    this.faqs[res.data.category].items[res.data.id] = res.data;
                                }
                            }
                            this.new_data.title = '';
                            this.new_data.content = '';
                            this._toastr.success('Successfully Created New Item', 'succes');
                        } else {
                            this._toastr.error('Unable to Create Item', 'Failed');
                        }
                    }, err => {
                        if (err.status === 401) {
                            this._global.clearSessions();
                        }
                    });
                } else {
                    this._toastr.warning('Content is required', 'Validation');
                }
            } else {
                this._toastr.warning('Title is required', 'Validation');
            }
        } else {
            this._toastr.warning('Category is required', 'Validation');
        }
    }

    update() {
        if (this.update_data.category.trim().length > 0) {
            if (this.update_data.title.trim().length > 0) {
                if (this.update_data.content.trim().length > 0 && this.update_data.content !== '<br>') {
                    this.Faq.update(this.update_data.id, this.update_data.category, this.update_data.title, this.update_data.content).subscribe(res => {
                        if (res.code === 200) {
                            delete this.faqs[this.preview_data.category][this.preview_data.id];
                            if (this.faqs[res.data.category] === undefined) {
                                this.categories.push(res.data.category);
                                this.faqs[res.data.category] = {
                                    items: [],
                                    count: 1,
                                };
                                this.faqs[res.data.category].items[res.data.id] = res.data;
                            } else {
                                this.faqs[res.data.category].items[res.data.id] = res.data;
                            }
                            this._toastr.success('Successfully Updated New Item', 'succes');
                        } else if (res.code === 400) {
                            this._toastr.error(res.message, 'Failed');
                        } else {
                            this._toastr.error('Unable to Update Item', 'Failed');
                        }
                    }, err => {
                        if (err.status === 401) {
                            this._global.clearSessions();
                        }
                    });
                } else {
                    this._toastr.warning('Content is required', 'Validation');
                }
            } else {
                this._toastr.warning('Title is required', 'Validation');
            }
        } else {
            this._toastr.warning('Category is required', 'Validation');
        }
    }

    editModal($category, $item_id) {
        this.preview_data = null;
        this.preview_data = this.faqs[$category].items[$item_id];

        this.update_data.id = $item_id;
        this.update_data.category = $category;
        this.update_data.title = this.preview_data.title;
        this.update_data.content = this.preview_data.content;
    }
}
