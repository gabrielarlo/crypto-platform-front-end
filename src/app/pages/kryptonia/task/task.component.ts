import { Component, OnInit } from '@angular/core';
import { TaskService } from '../../../services/model/task.service';
import { GlobalService } from '../../../services/util/global.service';

@Component({
    selector: 'app-task',
    templateUrl: './task.component.html',
    styleUrls: ['./task.component.scss']
})
export class TaskComponent implements OnInit {
    type = 'all';
    tasks = null;
    total = 0;
    page = 1;

    constructor(private _Task: TaskService, private _global: GlobalService) { }

    ngOnInit() {
        this.tasks = [];
        this.getList();
    }

    getList($limit = 50) {
        this._Task.getList(this.type, this.page, $limit).subscribe(res => {
            if (res.code === 200) {
                // TODO: To be refactor, Initial Only
                this.total = res.data.total;
                if (this.tasks.length === 0) {
                    this.tasks = res.data.items;
                } else {
                    this.tasks.concat(res.data.items);
                }
            }
        }, err => {
            if (err.status === 401) {
                this._global.clearSessions();
            }
        });
    }

    select($type) {
        this.tasks = [];
        this.type = $type;
        this.getList();
    }

    onScroll() {
        this.page++;
        this.getList();
    }

}
