import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TaskComponent } from './task.component';
import { AuthGuardService } from '../../auth/auth-guard.service';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MyPaginatorModule } from 'src/app/modules/my-paginator/my-paginator.module';
import { MomentModule } from 'ngx-moment';
import { MyLoadingModule } from 'src/app/modules/my-loading/my-loading.module';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

const routes: Routes = [
    {
        path: 'kryptonia',
        canActivate: [AuthGuardService],
        children: [
            {
                path: 'task',
                component: TaskComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        FormsModule,
        ReactiveFormsModule,
        MyPaginatorModule,
        MomentModule,
        MyLoadingModule,
        InfiniteScrollModule,
    ],
    declarations: [TaskComponent]
})
export class TaskModule { }
