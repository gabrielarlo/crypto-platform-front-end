import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../../services/util/global.service';
import { ChatService } from '../../../services/model/chat.service';
import { ContactInterface } from '../../../interfaces/ContactInterface';
import { MessageParamInterface } from 'src/app/interfaces/MessageParamInterface';
import { WebSocketService } from 'src/app/services/util/web-socket.service';

@Component({
    selector: 'app-chat',
    templateUrl: './chat.component.html',
    styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {
    user: any;
    avatar = '';

    contacts = null;
    contact_dir = null;
    conversations = null;
    selected_contact: ContactInterface = null;

    message = '';
    sending = false;

    constructor(protected _global: GlobalService, private _Chat: ChatService) {
        this.user = this._global.userinfo;
    }

    ngOnInit() {
        this.avatar = this._global.getAvatar(this.user.user_id);
        this.getContactList();
    }

    startListener() {
        // this._Chat.register(this.user.user_id);
        this._Chat.socket().on('user-connected', (data) => {
            if (typeof data === 'string') {
                data = JSON.parse(data);
                if (data.user_id === this.user.user_id) {
                    console.log('You are Connected');
                } else {
                    console.log('User Connected');
                    this.updateContactStatus(data.user_id);
                }
            } else {
                if (data.user_id === this.user.user_id) {
                    console.log('You are Connected');
                } else {
                    console.log('User Connected');
                    this.updateContactStatus(data.user_id);
                }
            }
        });
        this._Chat.socket().on('user-disconnected', (data) => {
            if (typeof data === 'string') {
                data = JSON.parse(data);
                if (data.user_id === this.user.user_id) {
                    console.log('You are Disconnected');
                } else {
                    console.log('User Disconnected');
                    this.updateContactStatus(data.user_id, false);
                }
            } else {
                if (data.user_id === this.user.user_id) {
                    console.log('You are Disconnected');
                } else {
                    console.log('User Disconnected');
                    this.updateContactStatus(data.user_id, false);
                }
            }
        });
        this._Chat.socket().on('chat', ($data) => {
            let is_seen = 0;
            if (this.selected_contact !== null) {
                if (this.selected_contact.user_id === $data.from && this.user.user_id === $data.to) {
                    is_seen = 1;
                    this.conversations.push($data);
                } else if (this.selected_contact.user_id === $data.to && this.user.user_id === $data.from) {
                    this.conversations.push($data);
                }
            }
            if (this.user.user_id === $data.to) {
                let key = this.contact_dir.listed[$data.from];
                if (key !== undefined) {
                    if (this.contacts.listed[key.key].latest_message === null) {
                        this.contacts.listed[key.key].latest_message = { message: $data.message, is_seen: is_seen };
                    } else {
                        this.contacts.listed[key.key].latest_message['message'] = $data.message;
                        this.contacts.listed[key.key].latest_message['is_seen'] = is_seen;
                    }
                } else {
                    key = this.contact_dir.unlisted[$data.from];
                    if (key !== undefined) {
                        if (this.contacts.unlisted[key.key].latest_message === null) {
                            this.contacts.unlisted[key.key].latest_message = { message: $data.message, is_seen: is_seen };
                        } else {
                            this.contacts.unlisted[key.key].latest_message.message = $data.message;
                            this.contacts.unlisted[key.key].latest_message.is_seen = is_seen;
                        }
                    }
                }

            }
            this._Chat.getLatestChat($data.from, $data.to, this.user.user_id, is_seen === 1 ? true : false).subscribe(res => {
                if (res.code === 200) {
                    if (res.data !== null) {
                        if ($data.from === this.user.user_id) {
                            // this.conversations.push(res.data);
                            this.contacts.listed[0].latest_message = {
                                message: 'You: ' + res.data.message,
                                is_seen: 1,
                                is_private: res.data.is_private,
                                created_at: res.data.created_at,
                            };
                        } else if ($data.to === this.user.user_id) {
                            this.getContactList(false);
                            // const key = this.contact_dir.listed[$data.from].key;
                            // this.contacts.listed[key].latest_message = {
                            //     message: res.data.message,
                            //     is_seen: res.data.is_seen,
                            //     is_private: res.data.is_private,
                            //     created_at: res.data.created_at,
                            // };
                        }
                    }
                }
            });
        });
    }

    updateContactStatus($user_id, $online = true) {
        if (this.contact_dir.listed[$user_id] !== undefined) {
            const key = this.contact_dir.listed[$user_id].key;
            if (this.contacts.listed[key] !== undefined) {
                if ($online) {
                    this.contacts.listed[key].online_status = {
                        text: 'online',
                        type: 'success',
                    };
                } else {
                    this.contacts.listed[key].online_status = {
                        text: 'offline',
                        type: 'default',
                    };
                }
            }
        }
    }

    updateAvatar(e) {
        this.avatar = 'assets/images/avatar.png';
    }

    getContactList($start = true) {
        this._Chat.getList(this.user.user_id).subscribe(res => {
            if (res.code === 200) {
                this.contacts = res.data.items;
                this.contact_dir = {
                    'listed': [],
                    'unlisted': [],
                };
                for (let i = 0; i < this.contacts.listed.length; i++) {
                    const element = this.contacts.listed[i];
                    this.contact_dir['listed'][element.contact_id] = { key: i };
                }
                for (let i = 0; i < this.contacts.unlisted.length; i++) {
                    const element = this.contacts.unlisted[i];
                    this.contact_dir['unlisted'][element.contact_id] = { key: i };
                }
                if ($start) {
                    this.startListener();
                }
            }
        });
    }

    openConversation($contact, $unlisted = false) {
        this.selected_contact = {
            user_id: $contact.contact_id,
            name: $contact.name,
            email: $contact.email,
            online_status: $contact.online_status
        };
        if ($contact.latest_message !== null) {
            $contact.latest_message['is_seen'] = 1;
        }
        if ($unlisted) {
            this.selected_contact = {
                user_id: $contact.sender_info.id,
                name: $contact.sender_info.name,
                email: $contact.sender_info.email,
                online_status: $contact.sender_info.online_status
            };
            if ($contact.sender_info.latest_message !== null) {
                $contact.sender_info.latest_message['is_seen'] = 1;
            }
        }
        this._Chat.getConversations(this.user.user_id, this.selected_contact.user_id).subscribe(res => {
            if (res.code === 200) {
                this.conversations = res.data.items.reverse();
            }
        });
    }

    keyPress($e) {
        if ($e.code === 'Enter') {
            if ($e.shiftKey || $e.ctrlKey) {
                this.message = this.message + '\n';
            } else {
                this.send();
            }
        }
    }

    removeBR($msg: string) {
        return $msg.replace(/<br\s*?>/gi, ' ');
    }

    send() {
        if (this.message.trim().length > 0) {
            this.message = this.message.replace(/(?:\r\n|\r|\n)/g, '<br>');
            this.sending = true;
            const data: MessageParamInterface = {
                from: this.user.user_id,
                to: this.selected_contact.user_id,
                message: this.message,
                is_private: 1,
            };
            this._Chat.send(data);
            this.sending = false;
            this.message = '';
            if (this.contacts.listed[0].contact_id !== this.selected_contact.user_id) {
                this.getContactList(false);
            }
        }
    }

}
