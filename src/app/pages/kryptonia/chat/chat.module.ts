import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MomentModule } from 'ngx-moment';
import { MyLoadingModule } from '../../../modules/my-loading/my-loading.module';
import { AuthGuardService } from '../../auth/auth-guard.service';
import { ChatComponent } from './chat.component';

const routes: Routes = [
    {
        path: 'kryptonia',
        canActivate: [AuthGuardService],
        children: [
            {
                path: 'chat',
                component: ChatComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        FormsModule,
        ReactiveFormsModule,
        MomentModule,
        MyLoadingModule,
    ],
    declarations: [ChatComponent]
})
export class ChatModule { }
