import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostComponent } from './post.component';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from '../../auth/auth-guard.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

const routes: Routes = [
    {
        path: 'kblog',
        canActivate: [AuthGuardService],
        children: [
            {
                path: 'posts',
                component: PostComponent
            }
        ]
    }
];

@NgModule({
  declarations: [PostComponent],
  imports: [
      CommonModule,
      RouterModule.forChild(routes),
      FormsModule,
      ReactiveFormsModule,
      InfiniteScrollModule,
  ]
})
export class PostModule { }
