import { Component, OnInit, OnDestroy } from '@angular/core';
import { BlogService } from 'src/app/services/model/blog.service';
import { ActivatedRoute } from '@angular/router';
import { GlobalService } from 'src/app/services/util/global.service';
import { ToastrService } from 'ngx-toastr';
import { Paginator } from 'src/app/modules/interface/Paginator';
import { BridgeService } from 'src/app/services/util/bridge.service';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-payout',
    templateUrl: './payout.component.html',
    styleUrls: ['./payout.component.scss']
})
export class PayoutComponent implements OnInit, OnDestroy {
    payouts = null;
    coin = '';

    selected = null;

    form_data = null;

    page = 1;

    blog_list = null;
    loading = false;
    selected_key = 0;
    subscription: Subscription;
    paginator: Paginator = {
        items: null,
        to_show: 3,
    };

    filter = {
        date: ''
    };

    constructor(private _Blog: BlogService, private _route: ActivatedRoute, private _global: GlobalService, private _toaster: ToastrService, private _bridge: BridgeService) {
        this._route.data.subscribe((data) => {
            this.coin = data.coin;
        });
    }

    ngOnInit() {
        this.filter.date = this.getDateToday();
        this.subscription = this._bridge.on('call-pager').subscribe(signal => { this.pager(signal.data.direction, signal.data.paginator.items); });
        this.subscription = this._bridge.on('call-loader').subscribe(signal => { this.loadOtherBlogs(this.selected_key, signal.data.page); });
        setTimeout(() => this.getList(true));
    }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }

    getDateToday(): string {
        const today = new Date();
        let dd = today.getDate().toString();
        let mm = (today.getMonth() + 1).toString();
        const yyyy = today.getFullYear().toString();
        if (today.getDate() < 10) {
            dd = '0' + today.getDate();
        }
        if (today.getMonth() < 9) {
            mm = '0' + mm;
        }

        return `${yyyy}-${mm}-${dd}`;
    }

    getList($new = false) {
        this._toaster.show('Loading List...');
        if (this.page === 1) {
            this.payouts = [];
        }
        if ($new) {
            this.payouts = [];
        }
        this._Blog.payoutList(this.coin, this.page, this.filter.date).subscribe((res) => {
            this._toaster.success('List Loaded');
            if (res.code === 200) {
                // TODO: Smooth
                this.payouts = this.payouts.concat(res.data.items);
            }
        });
    }

    onScroll() {
        this.page = this.page + 1;
        this.getList();
    }

    optionModal($key: number) {
        this.selected = this.payouts[$key];
        if (this.selected.rusgold_payout_info === null) {
            this.form_data = {
                blog_id: this.selected.blog_id,
                paid_date: '',
                waves_address: ''
            };
        } else {
            this.form_data = {
                blog_id: this.selected.blog_id,
                paid_date: this.selected.rusgold_payout_info.paid_date,
                waves_address: this.selected.rusgold_payout_info.waves_address
            };
        }
    }

    addPaid() {
        if (this.form_data.paid_date.trim().length === 0) {
            this._toaster.warning('Paid Date is requried');
            return false;
        }
        if (this.form_data.waves_address.trim().length === 0) {
            this._toaster.warning('Waves Address is requried');
            return false;
        }
        this._Blog.addRGPaid(this.coin, this.form_data).subscribe(res => {
            if (res.code === 201) {
                this._toaster.success(res.message);
                this.selected.rusgold_payout_info = res.data;
            } else {
                this._toaster.error(res.message);
            }
        });
    }

    editPaid() {
        if (this.form_data.paid_date.trim().length === 0) {
            this._toaster.warning('Paid Date is requried');
            return false;
        }
        if (this.form_data.waves_address.trim().length === 0) {
            this._toaster.warning('Waves Address is requried');
            return false;
        }
        this._Blog.updateRGPaid(this.coin, this.form_data).subscribe(res => {
            if (res.code === 200) {
                this._toaster.success(res.message);
                this.selected.rusgold_payout_info = res.data;
            } else {
                this._toaster.error(res.message);
            }
        });
    }

    pager($direction, $paginator) {
        let $page = 1;
        if ($paginator.current > 1) {
            if ($direction === 'start') {
                $page = 1;
            } else if ($direction === 'previous') {
                $page = $paginator.current - 1;
            }
        }
        if ($paginator.current < $paginator.total) {
            if ($direction === 'end') {
                $page = $paginator.total;
            } else if ($direction === 'next') {
                $page = $paginator.current + 1;
            }
        }
        this.loadOtherBlogs(this.selected_key, $page);
    }

    loadOtherBlogs($key: number, $page: number = 1) {
        this.selected_key = $key;
        this.selected = this.payouts[$key];
        this.loading = true;
        if ($page === 1) {
            this.blog_list = null;
        }
        this._Blog.userPosts(this.selected.user_id, $page, this.filter.date).subscribe(res => {
            this.blog_list = null;
            this.loading = false;
            if (res.code === 200) {
                this.blog_list = res.data.items;
                const paginator = {
                    total: Math.ceil(res.data.total / res.data.limit),
                    current: res.data.page,
                    buttons: [],
                };
                for (let index = paginator.current - this.paginator.to_show; index <= paginator.current + this.paginator.to_show; index++) {
                    if (index > 0 && index <= paginator.total) {
                        if (index === paginator.current) {
                            paginator.buttons.push({ num: index, class: 'info' });
                        } else {
                            paginator.buttons.push({ num: index, class: 'secondary' });
                        }
                    }
                }
                this.paginator = this._global.setPaginator(res.data.total, res.data.limit, $page);
            } else {
                this._toaster.error(res.message);
            }
        });
    }

    avatarURL($user_id: number) {
        return this._global.getAvatar($user_id);
    }

    blogURL($item: any) {
        return this._global.blogURL($item.tags_info[$item.tags_info.length - 1].tag_name, $item.user_info.username, $item.metadata);
    }

}
