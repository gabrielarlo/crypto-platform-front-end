import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PayoutComponent } from './payout.component';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from '../../auth/auth-guard.service';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MyLoadingModule } from 'src/app/modules/my-loading/my-loading.module';
import { MyPaginatorModule } from 'src/app/modules/my-paginator/my-paginator.module';

const routes: Routes = [
    {
        path: 'kblog',
        canActivate: [AuthGuardService],
        children: [
            {
                path: 'payouts',
                children: [
                    {
                        path: '',
                        redirectTo: '/kblog/payouts/superiorcoin',
                        pathMatch: 'full',
                    },
                    {
                        path: 'superiorcoin',
                        component: PayoutComponent,
                        data: { coin: 'superiorcoin' }
                    },
                    {
                        path: 'rusgold',
                        component: PayoutComponent,
                        data: { coin: 'rusgold' }
                    }
                ]
            }
        ]
    }
];

@NgModule({
  declarations: [PayoutComponent],
  imports: [
      CommonModule,
      RouterModule.forChild(routes),
      InfiniteScrollModule,
      FormsModule,
      ReactiveFormsModule,
      MyLoadingModule,
      MyPaginatorModule,
  ]
})
export class PayoutModule { }
