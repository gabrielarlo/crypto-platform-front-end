import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from '../../auth/auth-guard.service';

const routes: Routes = [
    {
        path: 'kblog',
        canActivate: [AuthGuardService],
        children: [
            {
                path: '',
                redirectTo: '/kblog/dashboard',
                pathMatch: 'full'
            },
            {
                path: 'dashboard',
                component: DashboardComponent
            }
        ]
    }
];

@NgModule({
  declarations: [DashboardComponent],
  imports: [
      CommonModule,
      RouterModule.forChild(routes),
  ]
})
export class DashboardModule { }
