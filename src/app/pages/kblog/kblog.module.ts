import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KblogComponent } from './kblog.component';
import { DashboardModule } from './dashboard/dashboard.module';
import { PostModule } from './post/post.module';
import { PayoutModule } from './payout/payout.module';

@NgModule({
  declarations: [KblogComponent],
  imports: [
      CommonModule,
      DashboardModule,
      PostModule,
      PayoutModule,
  ]
})
export class KblogModule { }
