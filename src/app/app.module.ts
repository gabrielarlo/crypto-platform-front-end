import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { WebStorageModule } from 'ngx-store';
import { AppComponent } from './app.component';
import { AuthModule } from './pages/auth/auth.module';
import { PortalModule } from './pages/portal/portal.module';
import { HttpClientModule } from '@angular/common/http';
import { ClipboardModule } from 'ngx-clipboard';
import { MomentModule } from 'ngx-moment';

import { NavigationComponent } from './layouts/navigation/navigation.component';
import { SidebarComponent } from './layouts/sidebar/sidebar.component';
import { KryptoniaModule } from './pages/kryptonia/kryptonia.module';
import { MyToastrModule } from './modules/my-toastr/my-toastr.module';
import { BridgeService } from './services/util/bridge.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { environment } from 'src/environments/environment';
import { KblogModule } from './pages/kblog/kblog.module';

const env = environment;
const config: SocketIoConfig = { url: env.SOCKET_SERVER, options: {
    path: '/kryptonia',
    forceNew: false,
} };

@NgModule({
    declarations: [
        AppComponent,
        NavigationComponent,
        SidebarComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        WebStorageModule,
        MyToastrModule,
        ClipboardModule,
        MomentModule,

        AuthModule,
        PortalModule,
        KryptoniaModule,
        BrowserAnimationsModule,

        SocketIoModule.forRoot(config),

        KblogModule,
    ],
    providers: [BridgeService],
    bootstrap: [AppComponent]
})
export class AppModule { }
