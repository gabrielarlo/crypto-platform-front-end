import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../pages/auth/auth.service';
import { Router } from '@angular/router';
import { GlobalService } from '../../services/util/global.service';
import { ChatService } from 'src/app/services/model/chat.service';

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
    authenticated = false;
    system = '';
    avatar = '';
    user: any;
    unread = 0;

    constructor(private _auth: AuthService, public _router: Router, private _global: GlobalService, private _Chat: ChatService) {
        if (this._auth.isAuthenticated()) {
            this.authenticated = true;
        }
        this.system = this._global.system;
        this.user = this._global.userinfo;
        if (this.user !== null) {
            this.avatar = this._global.getAvatar(this.user.user_id);
        }
    }

    ngOnInit() {
        this.getUnreadCount();
    }

    getUnreadCount() {
        this._Chat.unread(this.user.user_id).subscribe(res => {
            if (res.code === 200) {
                this.unread = res.data;
            }
        });
        this._Chat.socket().on('chat', ($data) => {
            console.log($data);
            if ($data.to === this.user.user_id) {
                this._Chat.unread(this.user.user_id).subscribe(res => {
                    if (res.code === 200) {
                        this.unread = res.data;
                    }
                });
            }
        });
    }

    updateAvatar(e) {
        // console.log(e);
        this.avatar = 'assets/images/avatar.png';
    }

    gotoMyAccount() {
        this._router.navigate([this._global.profileLink(this.user.user_id)]);
    }

}
