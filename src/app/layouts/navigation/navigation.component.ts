import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../pages/auth/auth.service';
import { GlobalService } from '../../services/util/global.service';

@Component({
    selector: 'app-navigation',
    templateUrl: './navigation.component.html',
    styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {
    authenticated = false;
    system = '';
    status = 'local';

    constructor(private _auth: AuthService, private _global: GlobalService) {
        if (this._auth.isAuthenticated()) {
            this.authenticated = true;
        }
        if (this._global.system.length > 0) {
            this.system = this._global.system;
        } else {
            this.system = 'portal';
        }
        this.getServerStatus();
    }

    ngOnInit() {
    }

    updateLogo() {
        if (this._global.system.length > 0) {
            this.system = this._global.system;
        } else {
            this.system = 'portal';
        }
    }

    getServerStatus() {
        this._global.publicGetMethod('/stat').subscribe(res => {
            if (res.code === 200) {
                this.status = res.data;
            } else {
                this.status = 'error';
            }
        }, err => {
            console.log(err);
            this.status = 'error';
        });
    }
}
