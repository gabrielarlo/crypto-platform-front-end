import { Injectable } from '@angular/core';
import { GlobalService } from '../util/global.service';

@Injectable({
    providedIn: 'root'
})
export class BlogService {
    base = '/kblog';

    constructor (private _global: GlobalService) { }

    public payoutList($coin, $page = 1, $date = '', $limit = 20) {
        const data = {
            page: $page,
            limit: $limit,
            date: $date,
        };
        return this._global.postMethod(`${this.base}/payout/${$coin}/list`, data);
    }

    public addRGPaid($coin, $data: RGPaidInterface) {
        return this._global.postMethod(`${this.base}/payout/${$coin}/add`, $data);
    }

    public updateRGPaid($coin, $data: RGPaidInterface) {
        return this._global.postMethod(`${this.base}/payout/${$coin}/update`, $data);
    }

    public userPosts($user_id, $page = 1, $date = '', $limit = 10) {
        const data = {
            user_id: $user_id,
            page: $page,
            date: $date,
            limit: $limit,
        };
        return this._global.postMethod(`${this.base}/user-posts`, data);
    }
}

export interface RGPaidInterface {
    blog_id: number;
    paid_date: string;
    waves_address: string;
}
