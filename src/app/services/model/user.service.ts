import { BankService } from './bank.service';
import { Injectable } from '@angular/core';
import { GlobalService } from '../util/global.service';
import { ReferralService } from './referral.service';

@Injectable({
    providedIn: 'root'
})
export class UserService {
    base = '/user';

    constructor(private _global: GlobalService, private Bank: BankService, private Referral: ReferralService) { }

    public getStat($scope) {
        const data = {
            scope: $scope,
            limit: 0,
        };
        return this._global.postMethod(this.base + '/list', data);
    }

    public getList($scope, $filter = '', $page = 1, $limit = 20) {
        const data = {
            scope: $scope,
            filter: $filter,
            page: $page,
            limit: $limit,
        };
        return this._global.postMethod(this.base + '/list', data);
    }

    public status($verified, $status, $agreed, $ban) {
        if ($ban === 1) {
            return { text: 'soft-banned', type: 'danger' };
        } else if ($ban === 2) {
            return { text: 'hard-banned', type: 'danger' };
        } else {
            if ($verified === 0) {
                return { text: 'unverified', type: 'info' };
            } else {
                if ($agreed === 0) {
                    return { text: 'unconfirmed', type: 'warning' };
                } else {
                    if ($status === 0) {
                        return { text: 'disabled', type: 'danger' };
                    } else {
                        return { text: 'active', type: 'success' };
                    }
                }
            }
        }
    }

    public verify($user_id) {
        const data = {
            user_id: $user_id,
        };
        return this._global.postMethod(this.base + '/verify', data);
    }

    public activate($user_id) {
        const data = {
            user_id: $user_id,
        };
        return this._global.postMethod(this.base + '/activate', data);
    }

    public disable($user_id, $reason) {
        const data = {
            user_id: $user_id,
            reason: $reason,
        };
        return this._global.postMethod(this.base + '/disable', data);
    }

    public banUpdate($user_id, $reason = '') {
        const data = {
            user_id: $user_id,
            ban_reason: $reason,
        };
        return this._global.postMethod(this.base + '/ban-update', data);
    }

    public account($user_id) {
        const data = {
            user_id: $user_id,
        };
        return this._global.postMethod(this.base + '/account', data);
    }

    public getBankBalances($user_id) {
        return this.Bank.balances($user_id);
    }

    public syncBalance($user_id, $password) {
        return this.Bank.sync($user_id, $password);
    }

    public getBankHistories($user_id, $scope, $type, $page = 1, $limit = 20) {
        return this.Bank.histories($user_id, $scope, $type, $page, $limit);
    }

    public basicWithdrawalAction($id, $action) {
        return this.Bank.basicWithdrawalAction($id, $action);
    }

    public bankHistoryStatus($status, $type = 'basic') {
        return this.Bank.status($status, $type);
    }

    public accountType($type) {
        if ($type === 9) {
            return {
                text: 'administrator',
                type: 'danger'
            };
        } else {
            return {
                text: 'regular',
                type: 'info'
            };
        }
    }

    public activities($user_id, $limit = 20, $page = 1) {
        const data = {
            user_id: $user_id,
            limit: $limit,
            page: $page,
        };
        return this._global.postMethod(this.base + '/activities', data);
    }

    public resendVerification($user_id) {
        const data = {
            user_id: $user_id,
        };
        return this._global.postMethod(this.base + '/resend-email-verification', data);
    }

    public referrals($user_id, $limit = 20, $page = 1) {
        return this.Referral.list($user_id, $limit, $page);
    }

    public referral_status($status) {
        return this.Referral.status($status);
    }
}
