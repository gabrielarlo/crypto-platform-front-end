import { Injectable } from '@angular/core';
import { GlobalService } from '../util/global.service';

@Injectable({
    providedIn: 'root'
})
export class SiteService {
    base = '/landing-page/affiliated-sites';

    constructor(private _global: GlobalService) { }

    public list($page = 1, $limit = 10) {
        const data = {
            page: $page,
            limit: $limit,
        };
        return this._global.postMethod(this.base + '/list', data);
    }

    public add($site_name, $site_url) {
        const data = {
            site_name: $site_name,
            site_url: $site_url,
        };
        return this._global.postMethod(this.base + '/add', data);
    }

    public update($id, $site_name, $site_url) {
        const data = {
            id: $id,
            site_name: $site_name,
            site_url: $site_url,
        };
        return this._global.postMethod(this.base + '/update', data);
    }

    public statusChange($id) {
        const data = {
            id: $id,
        };
        return this._global.postMethod(this.base + '/status-change', data);
    }
}
