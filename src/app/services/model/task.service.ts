import { Injectable } from '@angular/core';
import { GlobalService } from '../util/global.service';

@Injectable({
    providedIn: 'root'
})
export class TaskService {
    base = '/task';

    constructor(private _global: GlobalService) { }

    public getStat($type) {
        const data = {
            type: $type,
        };
        return this._global.postMethod(this.base + '/list', data);
    }

    public getList($type = 'all', $page = 1, $limit = 10) {
        const data = {
            type: $type,
            page: $page,
            limit: $limit,
        };
        return this._global.postMethod(this.base + '/list', data);
    }
}
