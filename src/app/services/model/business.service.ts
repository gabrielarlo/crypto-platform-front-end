import { Injectable } from '@angular/core';
import { GlobalService } from '../util/global.service';

@Injectable({
    providedIn: 'root'
})
export class BusinessService {
    base = '/landing-page/trusted-business';

    constructor(private _global: GlobalService) { }

    public list($page = 1, $limit = 10) {
        const data = {
            page: $page,
            limit: $limit,
        };
        return this._global.postMethod(this.base + '/list', data);
    }

    public add($business_name, $business_logo = null) {
        const data = {
            business_name: $business_name,
        };
        if ($business_logo !== null) {
            data['business_logo'] = $business_logo;
        }
        return this._global.postMethod(this.base + '/add', data);
    }

    public update($id, $business_name, $business_logo = null) {
        const data = {
            id: $id,
            business_name: $business_name,
        };
        if ($business_logo !== null) {
            data['business_logo'] = $business_logo;
        }
        return this._global.postMethod(this.base + '/update', data);
    }

    public changeLogo($id, $business_logo) {
        const formData: FormData = new FormData();
        formData.append('id', $id);
        formData.append('business_logo', $business_logo);
        return this._global.postMethod(this.base + '/change-logo', formData);
    }

    public statusChange($id) {
        const data = {
            id: $id,
        };
        return this._global.postMethod(this.base + '/status-change', data);
    }

    public getLogo($name, $original = true) {
        if ($original) {
            return this._global.env.BUSINESS_LOGO_ENDPOINT + '/original/' + $name;
        } else {
            return this._global.env.BUSINESS_LOGO_ENDPOINT + '/thumbnail/' + $name;
        }
    }
}
