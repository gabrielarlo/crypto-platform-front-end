import { Injectable } from '@angular/core';
import { GlobalService } from '../util/global.service';

@Injectable({
    providedIn: 'root'
})
export class ReferralService {
    base = '/referral';

    constructor(private _global: GlobalService) { }

    public list($user_id, $limit = 20, $page = 1) {
        const data = {
            user_id: $user_id,
            limit: $limit,
            page: $page,
        };
        return this._global.postMethod(this.base + '/list', data);
    }

    public status($status) {
        if ($status === 1) {
            return {text: 'pending', type: 'info'};
        } else if ($status === 2) {
            return {text: 'paid', type: 'success'};
        } else {
            return {text: 'canceled', type: 'danger'};
        }
    }
}
