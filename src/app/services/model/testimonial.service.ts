import { Injectable } from '@angular/core';
import { GlobalService } from '../util/global.service';

@Injectable({
    providedIn: 'root'
})
export class TestimonialService {
    base = '/landing-page/testimonial';

    constructor(private _global: GlobalService) { }

    public list($page = 1, $limit = 10) {
        const data = {
            page: $page,
            limit: $limit,
        };
        return this._global.postMethod(this.base + '/list', data);
    }

    public add($title, $content, $name) {
        const data = {
            title: $title,
            content: $content,
            name: $name,
        };
        return this._global.postMethod(this.base + '/add', data);
    }

    public update($id, $title, $content, $name) {
        const data = {
            id: $id,
            title: $title,
            content: $content,
            name: $name,
        };
        return this._global.postMethod(this.base + '/update', data);
    }

    public statusChange($id) {
        const data = {
            id: $id,
        };
        return this._global.postMethod(this.base + '/status-change', data);
    }
}
