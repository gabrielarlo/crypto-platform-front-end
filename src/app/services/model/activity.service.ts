import { Injectable } from '@angular/core';
import { GlobalService } from '../util/global.service';

@Injectable({
    providedIn: 'root'
})
export class ActivityService {
    base = '/activity';

    constructor(private _global: GlobalService) { }

    list($field = 'all', $page = 1, $limit = 10) {
        const data = {
            field: $field,
            page: $page,
            limit: $limit,
        };
        return this._global.postMethod(this.base + '/list', data);
    }
}
