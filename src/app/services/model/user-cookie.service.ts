import { Injectable } from '@angular/core';
import { GlobalService } from '../util/global.service';

@Injectable({
    providedIn: 'root'
})
export class UserCookieService {
    base = '/user/multi-account';

    constructor(private _global: GlobalService) { }

    public getList($page, $filter = '', $limit = 10) {
        const data = {
            page: $page,
            filter: $filter,
            limit: $limit,
        };
        return this._global.postMethod(this.base + '/list', data);
    }

    public getSimilar($cookie, $user_id = null) {
        const data = {
            cookie: $cookie,
            user_id: $user_id,
        };
        return this._global.postMethod(this.base + '/similar', data);
    }
}
