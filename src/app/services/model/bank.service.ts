import { Injectable } from '@angular/core';
import { GlobalService } from '../util/global.service';

@Injectable({
    providedIn: 'root'
})
export class BankService {
    base = '/bank';

    constructor(private _global: GlobalService) { }

    public masterBalance() {
        return this._global.postMethod(this.base + '/master-balance', {});
    }

    public balances($user_id) {
        const data = {
            user_id: $user_id,
        };
        return this._global.postMethod(this.base + '/balances', data);
    }

    public sync($user_id, $password) {
        const data = {
            user_id: $user_id,
            password: $password,
        };
        return this._global.postMethod(this.base + '/sync', data);
    }

    public histories($user_id, $scope, $type, $page = 1, $limit = 20) {
        const data = {
            user_id: $user_id,
            scope: $scope,
            type: $type,
            page: $page,
            limit: $limit,
        };
        return this._global.postMethod(this.base + '/history', data);
    }

    public generalHistories($scope, $type = 'deposit', $page = 1, $search = '', $limit = 10) {
        const data = {
            scope: $scope,
            type: $type,
            page: $page,
            search: $search,
            limit: $limit,
        };
        return this._global.postMethod(this.base + '/general/history', data);
    }

    public status($status, $scope = 'basic') {
        if ($scope === 'basic') {
            if ($status === 0) {
                return { text: 'unverified', type: 'warning' };
            } else if ($status === 1) {
                return { text: 'verified', type: 'info' };
            } else if ($status === 2) {
                return { text: 'processing', type: 'primary' };
            } else if ($status === 3) {
                return { text: 'complete', type: 'success' };
            } else if ($status === 7) {
                return { text: 'cancelled (7)', type: 'danger' };
            } else if ($status === 8) {
                return { text: 'locked', type: 'info' };
            } else if ($status === 9) {
                return { text: 'expired (9)', type: 'danger' };
            } else if ($status === 10) {
                return { text: 'for approval', type: 'primary' };
            } else if ($status === 11) {
                return { text: 'declined', type: 'danger' };
            } else if ($status === 14) {
                return { text: 'failed (14)', type: 'danger' };
            } else if ($status === 17) {
                return { text: 'failed (17)', type: 'danger' };
            } else {
                return { text: `code (${$status})`, type: 'info' };
            }
        } else if ($scope === 'task') {
            if ($status === 'on-hold') {
                return { text: 'on-hold', type: 'info' };
            } else if ($status === 'available') {
                return { text: 'completed', type: 'success' };
            } else {
                return { text: 'revoked', type: 'danger' };
            }
        }
    }

    public basicWithdrawalAction($id, $action) {
        const data = {
            id:  $id,
            action:  $action,
        };
        return this._global.postMethod(this.base + '/withdrawal-action', data);
    }
}
