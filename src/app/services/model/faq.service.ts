import { Injectable } from '@angular/core';
import { GlobalService } from '../util/global.service';

@Injectable({
    providedIn: 'root'
})
export class FaqService {
    base = '/faq';

    constructor(private _global: GlobalService) { }

    public list($page = 1, $limit = 10) {
        const data = {
            page: $page,
            limit: $limit,
        };
        return this._global.postMethod(this.base + '/list', data);
    }

    public deletedChange($item_id) {
        const data = {
            item_id: $item_id,
        };
        return this._global.postMethod(this.base + '/change-delete', data);
    }

    public add($category, $title, $content) {
        const data = {
            category: $category,
            title: $title,
            content: $content,
        };
        return this._global.postMethod(this.base + '/add', data);
    }

    public update($id, $category, $title, $content) {
        const data = {
            id: $id,
            category: $category,
            title: $title,
            content: $content,
        };
        return this._global.postMethod(this.base + '/update', data);
    }
}
