import { Injectable } from '@angular/core';
import { GlobalService } from '../util/global.service';
import { MessageParamInterface } from 'src/app/interfaces/MessageParamInterface';
import { WebSocketService } from '../util/web-socket.service';

@Injectable({
    providedIn: 'root'
})
export class ChatService {
    canSend = true;

    constructor(private _global: GlobalService, private _ws: WebSocketService) {}

    register($user_id: number) {
        this._ws.emit('register', { user_id: $user_id });
    }

    socket() {
        return this._ws.socket;
    }

    getList($user_id, $type = 'recent') {
        const data = {
            user_id: $user_id,
            type: $type,
        };
        return this._global.chatPostMethod('/contact/list', data);
    }

    getConversations($user_id, $other_user_id) {
        const data = {
            user_id: $user_id,
            other_user_id: $other_user_id,
        };
        return this._global.chatPostMethod('/message/conversation', data);
    }

    getLatestChat($from: number, $to: number, $me: number, $openned: boolean = false) {
        const data = {
            from: $from,
            to: $to,
            me: $me,
            openned: $openned,
        };
        return this._global.chatPostMethod('/message/latest', data);
    }

    send($data: MessageParamInterface) {
        if (this.canSend) {
            this.canSend = false;
            setTimeout(() => {
                this.canSend = true;
            }, 1000);
            const packet = {
                channel: 'chat',
                data: $data,
            };
            this._ws.emit('transmit', packet);
        }
    }

    unread($user_id: number) {
        const data = {
            user_id: $user_id,
        };
        return this._global.chatPostMethod('/message/unread', data);
    }
}
