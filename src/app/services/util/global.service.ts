import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';
import { SessionStorage, LocalStorage, SharedStorage } from 'ngx-store';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Paginator } from 'src/app/modules/interface/Paginator';

const ENV = environment;
const TOKEN_PREFIX = 'Bearer ';

@Injectable({
    providedIn: 'root'
})
export class GlobalService {
    @SessionStorage() api_token_session: any;
    @SessionStorage() user_session: any;
    @LocalStorage() api_token_local: any;
    @LocalStorage() user_local: any;

    @LocalStorage() storeit: any;
    @SharedStorage() shareit: any;

    @LocalStorage() chat_api_token: any;

    constructor(private _http: HttpClient) { }

    get host(): string {
        const host = ENV.HOST.substr(0, 15);
        return host;
    }

    get env(): any {
        return ENV;
    }

    get api_token(): string {
        return (this.api_token_session) ? this.api_token_session : this.api_token_local;
    }

    get userinfo(): any {
        return (this.user_session) ? this.user_session : this.user_local;
    }

    public saveSessions($token, $user, $chat_token, remember = false): void {
        this.api_token_session = $token;
        this.user_session = $user;
        this.api_token_session = $token;
        this.user_session = $user;
        this.chat_api_token = null;
        this.chat_api_token = $chat_token;
        if (remember === true) {
            this.api_token_local = $token;
            this.user_local = $user;
            this.api_token_local = $token;
            this.user_local = $user;
        }
    }

    public clearSessions(): void {
        this.api_token_session = '';
        this.api_token_local = '';
        this.user_session = '';
        this.user_local = '';
        this.api_token_session = '';
        this.api_token_local = '';
        this.user_session = '';
        this.user_local = '';

        this.storeit = '';
        this.storeit = '';
        window.location.replace('/login');
        console.log('Cleared');
    }

    public publicPostMethod(url: string, data: any): any {
        const headers = new HttpHeaders().set('Access-Control-Allow-Headers', 'X-Custom-Header');
        return this._http.post(ENV.HOST + ENV.API_ENDPOINT + url, data, { headers: headers });
    }

    public publicGetMethod(url: string, force = false): any {
        if (force === true) {
            return this._http.get(url);
        } else {
            return this._http.get(ENV.HOST + ENV.API_ENDPOINT + url);
        }
    }

    public postMethod(url: string, data: any): any {
        const token = TOKEN_PREFIX + this.api_token;
        let headers = new HttpHeaders().set('Authorization', token);
        headers = headers.set('Access-Control-Allow-Headers', 'X-Custom-Header');
        return this._http.post(ENV.HOST + ENV.API_ENDPOINT + url, data, { headers: headers });
    }

    public getMethod(url: string): any {
        const token = TOKEN_PREFIX + this.api_token;
        const headers = new HttpHeaders().set('Authorization', token);
        return this._http.get(ENV.HOST + ENV.API_ENDPOINT + url, { headers: headers });
    }

    public chatPostMethod(url: string, data: any): any {
        const token = TOKEN_PREFIX + this.chat_api_token;
        let headers = new HttpHeaders().set('Authorization', token);
        headers = headers.set('Access-Control-Allow-Headers', 'X-Custom-Header');
        return this._http.post(ENV.CHAT_SERVER_ENDPOINT + url, data, { headers: headers });
    }

    public setSystem($path): void {
        let path = $path;
        if ($path.indexOf('/') === 0) {
            path = $path.substr(1);
            if (path.indexOf('/') > 0) {
                path = path.substr(0, path.indexOf('/'));
            }
        }
        this.shareit = { system: path };
    }

    get system(): string {
        return (this.shareit.system) ? this.shareit.system : '';
    }

    public getAvatar($user_id) {
        return ENV.AVATAR_ENDPOINT + '/' + $user_id + '?resize=100x100';
        // return ENV.AVATAR_ENDPOINT + '/' + $user_id + '/avatar.png';
    }

    public getTaskImage($image) {
        if ($image === null) {
            return '/assets/task_default.png';
        }
        return ENV.TASK_IMAGE_ENDPOINT + '/' + $image;
    }

    public referralURL($ref_code) {
        return ENV.KRYPTONIA_HOST + '/?ref=' + $ref_code;
    }

    public profileLink($user_id) {
        return `/kryptonia/user/${$user_id}/account`;
    }

    public userBankLink($user_id) {
        return `/kryptonia/user/${$user_id}/bank-history`;
    }

    public setPaginator($total: number, $limit: number, $page: number, $scope: string = '', $type: string = '', $to_show: number = 3): Paginator {
        const paginator: Paginator = {
            items: {
                total: Math.ceil($total / $limit),
                current: $page,
                buttons: [],
            },
            to_show: $to_show,
            scope: '',
            type: ''
        };
        for (let index = paginator.items.current - paginator.to_show; index <= paginator.items.current + paginator.to_show; index++) {
            if (index > 0 && index <= paginator.items.total) {
                if (index === paginator.items.current) {
                    paginator.items.buttons.push({ num: index, class: 'info' });
                } else {
                    paginator.items.buttons.push({ num: index, class: 'secondary' });
                }
            }
        }
        paginator.scope = $scope;
        paginator.type = $type;
        return paginator;
    }

    public blogURL($tag, $username, $metadata) {
        return `https://kblog.io/article/${$tag}/@${$username}/${$metadata}`;
    }
}
