import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';

@Injectable({
    providedIn: 'root'
})
export class WebSocketService {

    constructor(private _socket: Socket) { }

    async emit($channel: string, $data: any) {
        this._socket.emit($channel, $data);
    }

    get socket(): any {
        return this._socket;
    }
}
