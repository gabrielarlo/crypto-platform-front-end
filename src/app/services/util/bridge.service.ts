import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class BridgeService {
    private subjects: Subject<any>[] = [];

    constructor() { }

    public publish(eventName: string, data: any = null) {
        this.subjects[eventName] = this.subjects[eventName] || new Subject();

        this.subjects[eventName].next({ data: data });
    }

    public on(eventName: string): Observable<any> {
        this.subjects[eventName] = this.subjects[eventName] || new Subject();

        return this.subjects[eventName].asObservable();
    }
}
