import { Component } from '@angular/core';
import { ChatService } from './services/model/chat.service';
import { GlobalService } from './services/util/global.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title = 'The Manager';
    url = '';

    user = null;

    constructor(private _Chat: ChatService, private _global: GlobalService) {
        this.user = this._global.userinfo;
        if (this.user !== null) {
            this._Chat.register(this.user.user_id);
        }
        this.url = window.location.pathname;
    }
}
