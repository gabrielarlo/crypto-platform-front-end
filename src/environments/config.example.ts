export const Config = {
    HOST: 'https://mapi.kryptonia.io',
    IMAGE_HOST: 'https://kimg.io',
    CHAT_HOST: 'https://kryptonia.io:2087',
    SOCKET_HOST: 'https://kryptonia.io:2096',
};
