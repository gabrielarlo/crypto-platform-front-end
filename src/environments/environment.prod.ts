import { Config } from './config';

const CONFIG = Config;

export const environment = {
    production: true,
    HOST: CONFIG.HOST,
    API_ENDPOINT: '/api',
    AVATAR_ENDPOINT: CONFIG.IMAGE_HOST + '/avatar',
    BUSINESS_LOGO_ENDPOINT: CONFIG.IMAGE_HOST + '/image/uploads/business_logo',
    TASK_IMAGE_ENDPOINT: CONFIG.IMAGE_HOST + '/image/uploads/tasks',
    CHAT_SERVER_ENDPOINT: CONFIG.CHAT_HOST + '/api',
    SOCKET_SERVER: CONFIG.SOCKET_HOST,

    KRYPTONIA_HOST: 'https://kryptonia.io',
};
